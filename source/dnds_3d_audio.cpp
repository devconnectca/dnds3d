#include "../include/dnds_3d_audio.h"

// Default constructor
AudioManager::AudioManager()
{

}

// Gets the singleton instance of the audio manager
AudioManager *AudioManager::getAudioManager()
{
	//If doesn't exist, create it
	if (!s_instance)
		s_instance = new AudioManager();
	//Return the singleton instance
	return s_instance;
}

// Deconstructor
AudioManager::~AudioManager()
{
	// Nothing to do
}

/*
	Loads a sound effect into memory
	- Constant values for the input
		parameters can be found above
	- Returns a reference to the sound
		effect to be used when playing
*/
SoundEffect *AudioManager::loadEffect(const char fxFile, const short unsigned int bitrate, const short unsigned int handle, const char volume, const char panning)
{
	mmLoadEffect(fxFile);

	SoundEffect* effect = new mm_sound_effect();

	effect->id = fxFile;
	effect->rate = bitrate;
	effect->handle = handle;
	effect->volume = volume;
	effect->panning = panning;

	return effect;
}

/*
	Loads a MOD file into audio memory
	- for modFile, place the defined
		version of your song file
		(Found in soundbank.h)
		ex: MOD_SONG
*/
void AudioManager::loadMod(const char modFile)
{
	mmLoad(modFile);
}

// Play a loaded mod
void AudioManager::playMod(const char modFile, const PlayMode playMode)
{
	mmStart(modFile, playMode);
}

// Pause a mod
void AudioManager::pauseMod()
{
	mmPause();
}

// Resume a paused mod
void AudioManager::resumeMod()
{
	mmResume();
}

// Stops a mod
void AudioManager::stopMod()
{
	mmStop();
}

// Initialize the maxmod module 
void AudioManager::init()
{
	// Set up the memory location
	mmInitDefaultMem((mm_addr)soundbank_bin);
}

// Play effect with default settings
void AudioManager::playEffect(const char effectFile)
{
	mmEffect(effectFile);
}

// Play effect with modified settings
void AudioManager::playEffect(SoundEffect *effect)
{
	mmEffectEx(effect);
}

void AudioManager::setfxVolume(const char volume)
{
	fx_volume = volume;
	mmSetEffectsVolume(volume);
}


void AudioManager::setModPitch(const char pitch)
{
	mod_pitch = pitch;
	mmSetModulePitch(pitch);
}

void AudioManager::setModTempo(const char tempo)
{
	mod_tempo = tempo;
	mmSetModuleTempo(tempo);
}

void AudioManager::setModVolume(const char volume)
{
	mod_volume = volume;
	mmSetModuleVolume(volume);
}

const char AudioManager::fxVolume()
{
	return fx_volume;
}

const char AudioManager::modPitch()
{
	return mod_pitch;
}

const char AudioManager::modTempo()
{
	return mod_tempo;
}

const char AudioManager::modVolume()
{
	return mod_volume;
}

