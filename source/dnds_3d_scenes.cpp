#include "../include/dnds_3d_scenes.h"

//Private function definition for rendering a vertex
void RenderVertex(Polygon *polygon, Vertex *vertex);

// SCENE

//Default constructor
Scene::Scene()
{
	//No base object supplied, create a new one
	_baseObject = new BaseObject();
	
	//Create a renderer object
	_renderer = new Renderer();

	//No update function set yet
	_updateFunction = 0;

	//Default frame time
	_frameTime = DEFAULT_FRAMETIME;

	// Frustum is null
	frustum_object = 0;
}

/*
	Scene Constructor
	@param BaseObject *base
		- User provides a pointer to an existing
			base object instead of letting the
			scene create its own
*/
Scene::Scene(BaseObject *base)
{
	//Set the base object reference
	_baseObject = base;

	//Create a renderer object
	_renderer = new Renderer();

	//No update function set yet
	_updateFunction = 0;

	//Default frame time
	_frameTime = DEFAULT_FRAMETIME;

	//Init the frustum
	initFrustum();
}

//Deconstructor
Scene::~Scene()
{
	//Perform a clean
	clean();

	//Clear the renderer
	if (_renderer)
	{
		delete _renderer;
	}
}

// Initializes the default frustum
void Scene::initFrustum(bool ortho)
{
	frustum_polygon = new Polygon();

	if (!ortho)
	{
		//Default frustum data
		const float far = 8.f;
		const float far_width = 7.2f;
		const float far_height = 5.5f;

		//FarTopLeft
		frustum[0] = Vector3f(-far_width, far_height, -far);
		//FarBottomLeft
		frustum[1] = Vector3f(-far_width, -far_height, -far);
		//FarBottomRight
		frustum[2] = Vector3f(far_width, -far_height, -far);
		//FarTopRight
		frustum[3] = Vector3f(far_width, far_height, -far);
		//NearTopLeft
		frustum[4] = Vector3f(-0.5, 0.4, 0);
		//NearBottomLeft
		frustum[5] = Vector3f(-0.5, -0.4, 0);
		//NearBottomRight
		frustum[6] = Vector3f(0.5, -0.4, 0);
		//NearTopRight
		frustum[7] = Vector3f(0.5, 0.4, 0);

		//Use frustum
		//Draw the frustum
		//Far
		frustum_polygon->addVertex(frustum[0], ColorRGB(255, 0, 0));
		frustum_polygon->addVertex(frustum[1]);
		frustum_polygon->addVertex(frustum[2]);
		frustum_polygon->addVertex(frustum[3], ColorRGB(255, 0, 0));
		//Near
		frustum_polygon->addVertex(frustum[7], ColorRGB(0, 0, 255));
		frustum_polygon->addVertex(frustum[6]);
		frustum_polygon->addVertex(frustum[5]);
		frustum_polygon->addVertex(frustum[4], ColorRGB(0, 0, 255));
		//Bottom
		frustum_polygon->addVertex(frustum[1], ColorRGB(0, 255, 0));
		frustum_polygon->addVertex(frustum[5]);
		frustum_polygon->addVertex(frustum[6]);
		frustum_polygon->addVertex(frustum[2], ColorRGB(0, 255, 0));
		//Left
		frustum_polygon->addVertex(frustum[4], ColorRGB(255, 0, 255));
		frustum_polygon->addVertex(frustum[5]);
		frustum_polygon->addVertex(frustum[1]);
		frustum_polygon->addVertex(frustum[0], ColorRGB(255, 0, 255));
		//Right
		frustum_polygon->addVertex(frustum[3], ColorRGB(255, 255, 0));
		frustum_polygon->addVertex(frustum[2]);
		frustum_polygon->addVertex(frustum[6]);
		frustum_polygon->addVertex(frustum[7], ColorRGB(255, 255, 0));
		//Top
		frustum_polygon->addVertex(frustum[4], ColorRGB(0, 255, 255));
		frustum_polygon->addVertex(frustum[0]);
		frustum_polygon->addVertex(frustum[3]);
		frustum_polygon->addVertex(frustum[7], ColorRGB(0, 255, 255));
	}
	else
	{
		//Far
		frustum_polygon->addVertex(Vector3f(0.f, 1.f, -10.f));
		frustum_polygon->addVertex(Vector3f(0.f, 0.f, -10.f));
		frustum_polygon->addVertex(Vector3f(1.f, 0.f, -10.f));
		frustum_polygon->addVertex(Vector3f(1.f, 1.f, -10.f));
		//Near
		frustum_polygon->addVertex(Vector3f(0.f, 1.f, 10.f));
		frustum_polygon->addVertex(Vector3f(0.f, 1.f, 10.f));
		frustum_polygon->addVertex(Vector3f(0.f, 1.f, 10.f));
		frustum_polygon->addVertex(Vector3f(0.f, 1.f, 10.f));
	}

	//Init the object to hold this frustum data
	frustum_object = new Object();
	//Add the data to the object
	frustum_object->addPolygon(frustum_polygon);
	//Frustums rotate based on camera position (so pos rotation is true)
	frustum_object->setPositionRotation(true);
	//Default to invisible
	frustum_object->setVisibility(false);

	//Add to our scene's base object
	baseObject()->addChild(frustum_object);
}

/*
	Clean
	- Used to completely clear the scene
	- This will delete all data stored in the
		scene's pointer variables.
*/
void Scene::clean()
{
	//Delete the base object
	if (_baseObject)
		delete _baseObject;
	//Set its reference to null
	_baseObject = 0;
	//Clean the renderer
	_renderer->clean();
}

void Scene::clearFrustum()
{
	if (frustum_object)
	{
		baseObject()->removeChild(frustum_object);
		delete frustum_object;
	}
	frustum_object = 0;
}

// Counts a frame towards updating
void Scene::countFrame()
{
	_frameCount++;
}

// Returns true if we have set the update function
bool Scene::hasUpdateFunction()
{
	return (_updateFunction != 0);
}

// Resets the frame count
void Scene::resetFrameCount()
{
	_frameCount = 0;
}

//Calls the update function
void Scene::update(float deltaTime)
{
	//Nullcheck the reference
	if (!hasUpdateFunction())
		return;

	//Call the function
	(*_updateFunction)(deltaTime);
}

/*
	Get BaseObject
	@output BaseObject *getBaseObject() 
		- The pointer to this scene's
			base object
*/
BaseObject *Scene::baseObject()
{
	return _baseObject;
}

/*
	Get Frustum Object
		@output Object *getFrustumObject()
			- The pointer to this scene's
				frustum object
*/
Object *Scene::frustumObject()
{
	return frustum_object;
}

// Returns frame time passed in milliseconds
const float Scene::frameCount()
{
	return (_frameCount * (1000.f / 60.f));
}

// Returns the current time per frame
const float Scene::frameTime()
{
	return _frameTime;
}

const float Scene::frustumBound(const int index)
{
	// Return the specified coordinate
	return frustum_object->bound(index);
}

/*
	Get Renderer
	@output Renderer *getRenderer()
*/
Renderer *Scene::renderer()
{
	return _renderer;
}

/*
	Get Room
	@output Room *getRoom()
*/
Room *Scene::room()
{
	return _room;
}

/*
	SetBaseObject
	- Sets this scene's base object
	- It is recommended that, if you used
		the scene's default constructor and
		then plan to set the base object here,
		you should call scene.clean() first.
		This will prevent memory leaks.
	@param BaseObject *base
		- User provides a pointer to the
			base object they wish to use for
			this scene
*/
void Scene::setBaseObject(BaseObject *base)
{
	_baseObject = base;
}

// Sets the time between frames (milliseconds)
void Scene::setFrameTime(const float frameTime)
{
	_frameTime = frameTime;
}

/*
	Set Room
	- Sets the current room for this scene
	- Pass null to disable indoor renderer
		optimizations
*/
void Scene::setRoom(Room *room)
{
	_room = room;
}

/*
	Set the update function to be called
		each frame
*/
void Scene::setUpdateFunction(void(*updateFunction)(float))
{
	_updateFunction = updateFunction;
}


// BASE OBJECT

//Default constructor
BaseObject::BaseObject()
{
	// No polygon data yet
	_polygons = 0;
	_polygonCount = 0;

	// No children yet
	_children = 0;
	_childCount = 0;

	// Scale is defaulted to 1.f
	_scale = Vector3f(1, 1, 1);

	// Rotates in place by default
	_positionRotate = true;

	// Visible by default
	_visible = true;

	// Default bound data
	for (int i = 0; i < 6; i++)
		bounds[i] = 0.f;

	// Bounds not set yet
	boundsSet = false;
}

//Deconstructor
BaseObject::~BaseObject()
{
	//Polygons are stored by reference
	if (_polygons)
	{
		delete _polygons;
	}
	//Children are stored by reference
	if (_children)
	{
		delete _children;
	}
}

/*
	Add Child
	@param child
		- Pointer to a child object to add
*/
void BaseObject::addChild(Object *child)
{
	// If child has no parent, we must be adding to the root
	if (child->parent() == 0)
		child->setRoot(this);

	// If first child, create the data
	if (!_children)
	{
		_childCount = 1;
		_children = new Object*[_childCount];
		_children[0] = child;
		return;
	}

	//Temp array to store the existing references
	Object **tempChildren = _children;

	//Create the reference array of children with new size
	_children = new Object*[++_childCount];

	//Copy over the old reference data
	for (int i = 0; i < _childCount - 1; i++)
	{
		_children[i] = tempChildren[i];
	}

	//Store the supplied child
	_children[_childCount - 1] = child;

	//Delete the temporary array
	delete tempChildren;
}

/*
	Add Polygon
	@param polygon
		- Pointer to an existing polygon
			(If this polygon ever gets 
			deleted, this object will
			break your scene)
*/
void BaseObject::addPolygon(Polygon *polygon)
{
	//If first polygon...
	if (!_polygons)
	{
		_polygonCount = 1;
		_polygons = new Polygon*[_polygonCount];
		_polygons[0] = polygon;
		return;
	}

	//Temp array to store the existing polygon references
	Polygon **tempPolygons = _polygons;

	//Create the reference array of polygons with new size
	_polygons = new Polygon*[++_polygonCount];

	//Copy over the old reference data
	for (int i = 0; i < _polygonCount - 1; i++)
	{
		_polygons[i] = tempPolygons[i];
	}

	//Store the supplied polygon
	_polygons[_polygonCount - 1] = polygon;

	//Delete the temporary array
	delete tempPolygons;
}

/*
	Calculate Vertices
	- forces all polygons in this object to
		recalculate their vertices
	- also forces all child objects to do the
		same
*/
void BaseObject::calculateVertices()
{
	//First we reset all vertex bools
	for (int i = 0; i < _polygonCount; i++)
	{
		for (int j = 0; j < _polygons[i]->vertexCount(); j++)
		{
			_polygons[i]->vertices()[j]->setCalculated(false);
		}
	}
	for (int k = 0; k < _childCount; k++)
	{
		for (int i = 0; i < _children[k]->polygonCount(); i++)
		{
			for (int j = 0; j < _children[j]->polygons()[i]->vertexCount(); j++)
			{
				_children[k]->polygons()[i]->vertices()[j]->setCalculated(false);
			}
		}
	}

	//Baes object passes these values in
	Vector3f scale(1.f, 1.f, 1.f);
	Vector3i rotation(0, 0, 0);
	Vector3f position(0.f, 0.f, 0.f);
	calculateVertices(scale, rotation, position);
}

// Recalculates the bounds based on a vertex position
void BaseObject::calculateBounds(Vector3f *position)
{

	if (!boundsSet)
	{
		// Bounds have never been set before
		boundsSet = true;
		
		// Set all bounds to position
		setBound(0, position->x);
		setBound(1, position->x);
		setBound(2, position->y);
		setBound(3, position->y);
		setBound(4, position->z);
		setBound(5, position->z);

		return;
	}

	// Bounds have been set before, test and update

	// Min/max X
	if (position->x < bound(0))
		setBound(0, position->x);
	if (position->x > bound(1))
		setBound(1, position->x);
	// Min/max Y
	if (position->y < bound(2))
		setBound(2, position->y);
	if (position->y > bound(3))
		setBound(3, position->y);
	// Min/max Z
	if (position->z < bound(4))
		setBound(4, position->z);
	if (position->z > bound(5))
		setBound(5, position->z);
}

// Removes child matching given address
void BaseObject::removeChild(Object * child)
{
	// Check all children
	for (int i = 0; i < _childCount; i++)
	{
		if (_children[i] == child)
		{
			removeChild(i);
			return;
		}
	}

	// No match
	return;
}

// Removes child object at given index
void BaseObject::removeChild(const int index)
{
	//Verify index is in bounds
	if (index < 0 || index >= _childCount)
		return;

	// Check if only object in here
	if (_childCount == 1)
	{
		delete _children;
		_children = 0;
		_childCount = 0;
		return;
	}

	// Temp array
	Object **temp_children = _children;

	// Recreate children array
	_children = new Object*[_childCount - 1];

	// Copy over all objects except the remove index
	for (int i = 0; i < _childCount; i++)
	{
		if (i < index)
			_children[i] = temp_children[i];
		else if (i > index)
			_children[i - 1] = temp_children[i];
	}

	// Remove the temp array
	delete temp_children;

	// Adjust child count
	_childCount--;
}

// Removes polygon matching given address
void BaseObject::removePolygon(Polygon *polygon)
{
	// Check all polygons
	for (int i = 0; i < _polygonCount; i++)
	{
		if (_polygons[i] == polygon)
		{
			removePolygon(i);
			return;
		}
	}

	// No match
	return;
}

// Removes polygon at given index
void BaseObject::removePolygon(const int index)
{
	//Verify index is in bounds
	if (index < 0 || index >= _polygonCount)
		return;

	// Check if only polygon in here
	if (_polygonCount == 1)
	{
		delete _polygons;
		_polygons = 0;
		_polygonCount = 0;
		return;
	}

	// Temp array
	Polygon **temp_polygons = _polygons;

	// Recreate polygon array
	_polygons = new Polygon*[_polygonCount - 1];

	// Copy over all objects except the remove index
	for (int i = 0; i < _polygonCount; i++)
	{
		if (i < index)
			_polygons[i] = temp_polygons[i];
		else if (i > index)
			_polygons[i - 1] = temp_polygons[i];
	}

	// Remove the temp array
	delete temp_polygons;

	// Adjust polygon count
	_polygonCount--;
}

/*
	Privately handled version includes the scale, rotation and translations
*/
void BaseObject::calculateVertices(Vector3f scale, Vector3i rotation, Vector3f position)
{
	//Reset bounds to not set
	boundsSet = false;

	// Apply this object's transform data
	// Scale
	scale.x *= _scale.x;
	scale.y *= _scale.y;
	scale.z *= _scale.z;
	// Rotation
	rotation.x += _rotation.x;
	rotation.y += _rotation.y;
	rotation.z += _rotation.z;
	// Position
	position.x += _position.x;
	position.y += _position.y;
	position.z += _position.z;

	// Pointer to current polygon to recalculate
	Polygon *polygon;
	// Pointer to current vertex to recalculate
	Vertex *vertex;
	// Vector3f for the vertex data
	Vector3f renderPosition;
	Vector3f tempPosition;
	// Angle of rotation
	float angle;

	// Recalcualate polygon vertices
	for (int p = 0; p < _polygonCount; p++)
	{
		// Store reference
		polygon = _polygons[p];

		// Loop through all vertices and apply this object's data to it
		for (int v = 0; v < polygon->vertexCount(); v++)
		{
			// Store reference
			vertex = polygon->vertices()[v];

			// Get the vertex's original position
			renderPosition = vertex->position();

			// First, apply scaling
			renderPosition.x *= scale.x;
			renderPosition.y *= scale.y;
			renderPosition.z *= scale.z;

			// If we don't want to rotate in place, apply translation first
			if (!positionRotate())
			{
				// Now we translate
				renderPosition.x += position.x;
				renderPosition.y += position.y;
				renderPosition.z += position.z;
			}

			/* Next, rotations
				3D axis rotations
				X:	x' = x
					y' = ycos@ - zsin@
					z' = ysin@ + zcos@

				Y:	x' = xcos@ + zsin@
					y' = y
					z' = -xsin@ + zcos@

				Z:	x' = xcos@ - ysin@
					y' = xsin@ + ycos@
					z' = z
			*/
			// First, rotate the X axis
			if (rotation.x != 0)
			{
				// Keep within the 360 coordinate circle
				if (rotation.x >= 360)
					angle = (float)(rotation.x % 360);
				else
				{
					//Mod doesn't work for negative angles
					angle = (float)rotation.x;
					while (angle < 0)
						angle += 360;
				}

				// Convert angle to radians
				angle = (float)PI * (angle / 180.f);

				// Store temp values
				tempPosition.x = renderPosition.x;
				tempPosition.y = renderPosition.y;
				tempPosition.z = renderPosition.z;

				// Calculate the new position
				renderPosition.x = tempPosition.x;
				renderPosition.y = tempPosition.y * cosf(angle) -
					tempPosition.z * sinf(angle);
				renderPosition.z = tempPosition.y * sinf(angle) +
					tempPosition.z * cosf(angle);
			}
			// Next, rotate the Y axis
			if (rotation.y != 0)
			{
				// Keep within the 360 coordinate circle
				if (rotation.y >= 360)
					angle = (float)(rotation.y % 360);
				else
				{
					//Mod doesn't work for negative angles
					angle = (float)rotation.y;
					while (angle < 0)
						angle += 360;
				}

				// Convert angle to radians
				angle = (float)PI * (angle / 180.f);

				// Store temp values
				tempPosition.x = renderPosition.x;
				tempPosition.y = renderPosition.y;
				tempPosition.z = renderPosition.z;

				// Calculate the new position
				renderPosition.x = tempPosition.x * cosf(angle) -
					tempPosition.z * sinf(angle);
				renderPosition.y = tempPosition.y;
				renderPosition.z = tempPosition.x * sinf(angle) +
					tempPosition.z * cosf(angle);
			}
			// Lastly, rotate the Z axis
			if (rotation.z != 0)
			{
				// Keep within the 360 coordinate circle
				if (rotation.z >= 360)
					angle = (float)(rotation.z % 360);
				else
				{
					//Mod doesn't work for negative angles
					angle = (float)rotation.z;
					while (angle < 0)
						angle += 360;
				}

				// Convert angle to radians
				angle = (float)PI * (angle / 180.f);

				// Store temp values
				tempPosition.x = renderPosition.x;
				tempPosition.y = renderPosition.y;
				tempPosition.z = renderPosition.z;

				// Calculate the new position
				renderPosition.x = tempPosition.x * cosf(angle) -
					tempPosition.y * sinf(angle);
				renderPosition.y = tempPosition.x * sinf(angle) +
					tempPosition.y * cosf(angle);
				renderPosition.z = tempPosition.z;
			}

			//If we want to rotate in place
			if (positionRotate())
			{
				// Now we translate
				renderPosition.x += position.x;
				renderPosition.y += position.y;
				renderPosition.z += position.z;
			}

			// Store the calculated render position
			vertex->setRenderPosition(renderPosition);

			// Calculate the bounds of this object
			calculateBounds(&renderPosition);

		} // End of vertex loop
	} // End of polygon loop

	// Done with polygons, now call this on all children
	for (int c = 0; c < _childCount; c++)
	{
		_children[c]->calculateVertices(scale, rotation, position);
	}
}

/*
	Returns the bound at the given index, 0 if out of bounds
		0: smallest X
		1: largest X
		2: smallest Y
		3: largest Y
		4: smallest Z
		5: largest Z
*/
const float BaseObject::bound(const int bound_index)
{
	// Verify index bounds
	if (bound_index < 0 || bound_index > 5)
		return 0.f;

	// Return the bound
	return bounds[bound_index];
}

// Gets the number of children of this object
const int BaseObject::childCount()
{
	return _childCount;
}

// Gets the children reference array
Object **BaseObject::children()
{
	return _children;
}

// Gets the number of polygons in this object
const int BaseObject::polygonCount()
{
	return _polygonCount;
}

// Gets the polygon reference array
Polygon **BaseObject::polygons()
{
	return _polygons;
}

// Returns the value of position rotate
const bool BaseObject::positionRotate()
{
	return _positionRotate;
}

/*
	Get Position
	@output
		- constant copy of the object's
			position from its parent (or origin)
*/
const Vector3f BaseObject::position()
{
	return _position;
}

/*
	Get Scale
	@output
		- constant copy of this object's scale value
*/
const Vector3f BaseObject::scale()
{
	return _scale;
}

/*
	Get Rotation
	@output
		- constant copy of this object's rotation
*/
const Vector3i BaseObject::rotation()
{
	return _rotation;
}

// Gets the visibility of this object
const bool BaseObject::visible()
{
	return _visible;
}

/*
	Sets the bound at the given index
		0: smallest X
		1: largest X
		2: smallest Y
		3: largest Y
		4: smallest Z
		5: largest Z
*/
void BaseObject::setBound(const int index, const float value)
{
	// Verify index is in bounds
	if (index < 0 || index > 5)
		return;

	//Set value
	bounds[index] = value;
}

/*
	Set Position Rotation
	@ param inplace
		- if true, rotates in place,
			otherwise, rotates based on
			object's position
		- default is true
*/
void BaseObject::setPositionRotation(const bool inplace)
{
	_positionRotate = inplace;
}

/*
	Set Position
	@param position
		- the position from this object's
			parent to apply to all of
			this object's geometry and
			children
*/
void BaseObject::setPosition(const Vector3f position)
{
	_position = Vector3f(position);
}

/*
	Set Scale
	@param scale
		- the value to multiply each vertex 
			in this object and all of its
			children before rendering
*/
void BaseObject::setScale(const Vector3f scale)
{
	_scale = scale;
}

/*
	Set Rotation
	@param rotation
		- the value to rotate each vertice about
			the object's origin by. This is also
			applied to all children
*/
void BaseObject::setRotation(const Vector3i rotation)
{
	_rotation = rotation;
}

// Sets the visibility of this object
void BaseObject::setVisibility(const bool visible)
{
	_visible = visible;
}


// OBJECT

void Object::calculateBounds(Vector3f * position)
{
	//Update this object's bounds
	BaseObject::calculateBounds(position);

	//Update the parent's
	if (_parent)
		_parent->calculateBounds(position);
	else if (_root)
		_root->calculateBounds(position);
}

void Object::calculateVertices(Vector3f scale, Vector3i rotation, Vector3f position)
{
	//Reset bounds to not set
	boundsSet = false;

	// Apply this object's transform data
	// Scale
	scale.x *= _scale.x;
	scale.y *= _scale.y;
	scale.z *= _scale.z;
	// Rotation
	rotation.x += _rotation.x;
	rotation.y += _rotation.y;
	rotation.z += _rotation.z;
	// Position
	position.x += _position.x;
	position.y += _position.y;
	position.z += _position.z;

	// Pointer to current polygon to recalculate
	Polygon *polygon;
	// Pointer to current vertex to recalculate
	Vertex *vertex;
	// Vector3f for the vertex data
	Vector3f renderPosition;
	Vector3f tempPosition;
	// Angle of rotation
	float angle;

	// Recalcualate polygon vertices
	for (int p = 0; p < _polygonCount; p++)
	{
		// Store reference
		polygon = _polygons[p];

		// Loop through all vertices and apply this object's data to it
		for (int v = 0; v < polygon->vertexCount(); v++)
		{
			// Store reference
			vertex = polygon->vertices()[v];

			// Get the vertex's original position
			renderPosition = vertex->position();

			// First, apply scaling
			renderPosition.x *= scale.x;
			renderPosition.y *= scale.y;
			renderPosition.z *= scale.z;

			// If we don't want to rotate in place, apply translation first
			if (!positionRotate())
			{
				// Now we translate
				renderPosition.x += position.x;
				renderPosition.y += position.y;
				renderPosition.z += position.z;
			}

			/* Next, rotations
			3D axis rotations
			X:	x' = x
			y' = ycos@ - zsin@
			z' = ysin@ + zcos@

			Y:	x' = xcos@ + zsin@
			y' = y
			z' = -xsin@ + zcos@

			Z:	x' = xcos@ - ysin@
			y' = xsin@ + ycos@
			z' = z
			*/
			// First, rotate the X axis
			if (rotation.x != 0)
			{
				// Keep within the 360 coordinate circle
				if (rotation.x >= 360)
					angle = (float)(rotation.x % 360);
				else
				{
					//Mod doesn't work for negative angles
					angle = (float)rotation.x;
					while (angle < 0)
						angle += 360;
				}

				// Convert angle to radians
				angle = (float)PI * (angle / 180.f);

				// Store temp values
				tempPosition.x = renderPosition.x;
				tempPosition.y = renderPosition.y;
				tempPosition.z = renderPosition.z;

				// Calculate the new position
				renderPosition.x = tempPosition.x;
				renderPosition.y = tempPosition.y * cosf(angle) -
					tempPosition.z * sinf(angle);
				renderPosition.z = tempPosition.y * sinf(angle) +
					tempPosition.z * cosf(angle);
			}
			// Next, rotate the Y axis
			if (rotation.y != 0)
			{
				// Keep within the 360 coordinate circle
				if (rotation.y >= 360)
					angle = (float)(rotation.y % 360);
				else
				{
					//Mod doesn't work for negative angles
					angle = (float)rotation.y;
					while (angle < 0)
						angle += 360;
				}

				// Convert angle to radians
				angle = (float)PI * (angle / 180.f);

				// Store temp values
				tempPosition.x = renderPosition.x;
				tempPosition.y = renderPosition.y;
				tempPosition.z = renderPosition.z;

				// Calculate the new position
				renderPosition.x = tempPosition.x * cosf(angle) -
					tempPosition.z * sinf(angle);
				renderPosition.y = tempPosition.y;
				renderPosition.z = tempPosition.x * sinf(angle) +
					tempPosition.z * cosf(angle);
			}
			// Lastly, rotate the Z axis
			if (rotation.z != 0)
			{
				// Keep within the 360 coordinate circle
				if (rotation.z >= 360)
					angle = (float)(rotation.z % 360);
				else
				{
					//Mod doesn't work for negative angles
					angle = (float)rotation.z;
					while (angle < 0)
						angle += 360;
				}

				// Convert angle to radians
				angle = (float)PI * (angle / 180.f);

				// Store temp values
				tempPosition.x = renderPosition.x;
				tempPosition.y = renderPosition.y;
				tempPosition.z = renderPosition.z;

				// Calculate the new position
				renderPosition.x = tempPosition.x * cosf(angle) -
					tempPosition.y * sinf(angle);
				renderPosition.y = tempPosition.x * sinf(angle) +
					tempPosition.y * cosf(angle);
				renderPosition.z = tempPosition.z;
			}

			//If we want to rotate in place
			if (positionRotate())
			{
				// Now we translate
				renderPosition.x += position.x;
				renderPosition.y += position.y;
				renderPosition.z += position.z;
			}

			// Store the calculated render position
			vertex->setRenderPosition(renderPosition);

			// Calculate the bounds of this object
			calculateBounds(&renderPosition);

		} // End of vertex loop
	} // End of polygon loop

	  // Done with polygons, now call this on all children
	for (int c = 0; c < _childCount; c++)
	{
		_children[c]->calculateVertices(scale, rotation, position);
	}
}

// Default constructor
Object::Object()
{
	// Null out parent refs
	_parent = 0;
	_root = 0;
}

/*
	Add child to non-base object
*/
void Object::addChild(Object *child)
{
	// This object is the parent
	child->setParent(this);
	// Call the base class implementation
	BaseObject::addChild(child);
}

// Gets the object reference to the parent
Object *Object::parent()
{
	return _parent;
}

// Gets the base object reference to the root
BaseObject *Object::root()
{
	return _root;
}

// Sets the object reference to the parent
void Object::setParent(Object *parent)
{
	_parent = parent;
	_root = 0;
}

// Sets the base object reference to the root
void Object::setRoot(BaseObject *root)
{
	_root = root;
	_parent = 0;
}

// POLYGON

//Default constructor
Polygon::Polygon()
{
	//Null out texture reference
	_texture = 0;

	//No vertices yet
	_vertexCount = 0;
	_vertices = 0;

	//Default data
	_type = GL_QUADS;
	_backRender = false;
}

//Copy constructor
Polygon::Polygon(const Polygon &polygon)
{
	//We can copy the texture as a reference
	_texture = polygon._texture;

	//Copy the data
	_type = polygon._type;
	_backRender = polygon._backRender;

	//No vertices yet
	_vertexCount = 0;
	_vertices = 0;

	//We will add vertices using the standard vertex addition functions
	for (int i = 0; i < polygon._vertexCount; i++)
	{
		//All vertices have position
		Vector3f position(polygon._vertices[i]->position());
		
		//We have to check if texture/color/no extra data is set
		if (polygon._vertices[i]->texturePositionSet())
		{
			Vector2i texture(polygon._vertices[i]->texturePosition());
			addVertex(position, texture);
		}
		else if (polygon._vertices[i]->colorSet())
		{
			ColorRGB color(polygon._vertices[i]->color());
			addVertex(position, color);
		}
		else
		{
			addVertex(position);
		}
	}
}

/*
	Polygon constructor
	@param type
		- if you are not using GL_QUADS,
			specify the type of object
			you wish OpenGL to render
			this polygon with
*/
Polygon::Polygon(const POLYGON_TYPE type)
{
	//Store type
	_type = type;

	//No vertices yet
	_vertexCount = 0;
	_vertices = 0;

	//Default backrender
	_backRender = false;
}

/*
	Polygon constructor
	@param texture
		- reference to a texture that
			will be rendered over this
			polygon
*/
Polygon::Polygon(Texture *texture)
{
	//Set our texture reference
	_texture = texture;

	//No vertices yet
	_vertexCount = 0;
	_vertices = 0;

	//Default data
	_type = GL_QUADS;
	_backRender = false;
}

/*
	Polygon constructor
	@param texture
		- reference to a texture that
			will be rendered over this
			polygon
	@param type
		- if you are not using GL_QUADS,
			specify the type of object
			you wish OpenGL to render
			this polygon with
*/
Polygon::Polygon(Texture *texture, const POLYGON_TYPE type)
{
	//Set our texture reference
	_texture = texture;

	//No vertices yet
	_vertexCount = 0;
	_vertices = 0;

	//Store the type
	_type = type;

	//Default backrender
	_backRender = false;
}

//Deconstructor
Polygon::~Polygon()
{
	/*
		Since we only ever reference
		an existing texture. We do not
		delete it when the polygon is
		deleted.
	*/

	//We do, however, remove vertex data
	if (_vertices)
	{
		for (int i = 0; i < _vertexCount; i++)
		{
			// Don't delete referenced ones
			if(!_vertices[i]->referenced())
				delete _vertices[i];
		}
		delete _vertices;
	}
}

// Private Vertex Function
void Polygon::addVertex(Vertex *vertex)
{
	if (!_vertices)
	{
		//Initialize the vertex array and store our vertex
		_vertexCount = 1;
		_vertices = new Vertex*[_vertexCount];
		_vertices[0] = vertex;
		return;
	}

	//Temp array to store the existing vertex references
	Vertex **tempVertices = _vertices;

	//Create the reference array of vertices with new size
	_vertices = new Vertex*[++_vertexCount];

	//Copy over the old reference data
	for (int i = 0; i < _vertexCount - 1; i++)
	{
		_vertices[i] = tempVertices[i];
	}

	//Store the supplied vertex
	_vertices[_vertexCount - 1] = vertex;

	//Delete the temporary array
	delete tempVertices;
}

// Public-Facing Vertex Functions

/*
	Add Vertex
	- adds a new vertex to this polygon
	@param position
		- the position relative to this
			polygon's parent object
*/
void Polygon::addVertex(const Vector3f position)
{
	Vertex *vertex = new Vertex();
	vertex->setPosition(position);
	addVertex(vertex);
}

/*
	Add Vertex
	- adds a new vertex to this polygon
	@param position
		- the position relative to this
			polygon's parent object
	@param texturePosition
		- the position on the 2D texture
			that maps to this vertex
*/
void Polygon::addVertex(const Vector3f position, const Vector2i texturePosition)
{
	Vertex *vertex = new Vertex();
	vertex->setPosition(position);
	vertex->setTexturePosition(texturePosition);
	addVertex(vertex);
}

/*
	Add Vertex
	- adds a new vertex to this polygon
	@param position
		- the position relative to this
			polygon's parent object
	@param color
		- when rendering this pixel, will set
			OpenGL color to this color
*/
void Polygon::addVertex(const Vector3f position, const ColorRGB color)
{
	Vertex *vertex = new Vertex();
	vertex->setPosition(position);
	vertex->setColor(color);
	addVertex(vertex);
}

/*
	Add Vertex
	- adds a new vertex to this polygon
	@param position
		- the position relative to this
			polygon's parent object
	@param texturePosition
		- the position on the 2D texture
			that maps to this vertex
	@param color
		- when rendering this pixel, will set
			OpenGL color to this color
*/
void Polygon::addVertex(const Vector3f position, const Vector2i texturePosition, const ColorRGB color)
{
	Vertex *vertex = new Vertex();
	vertex->setPosition(position);
	vertex->setTexturePosition(texturePosition);
	vertex->setColor(color);
	addVertex(vertex);
}

/*
	Get Back Render
	@output
		- true: render the back
		- false: don't render the back
*/
const bool Polygon::backRender()
{
	return _backRender;
}

/*
	Get Polygon Type
	@output
		- the polygon type
*/
const POLYGON_TYPE Polygon::type()
{
	return _type;
}

/*
	Get Texture
	@output
		- the referenced texture
*/
Texture *Polygon::texture()
{
	return _texture;
}

/*
	Get Vertices
	@output
		- reference of vertex array
*/
Vertex **Polygon::vertices()
{
	return _vertices;
}

/*
	Get Vertex Count
	@output
		- number of vertices
*/
const int Polygon::vertexCount()
{
	return _vertexCount;
}

/*
	Set Back Render
	- change the back render setting
	@param backRender
		- true: Render both sides
		- false: Only render the normals given
*/
void Polygon::setBackRender(const bool backRender)
{
	_backRender = backRender;
}

/*
	Set Texture
	- sets the texture reference variable
	@param texture
		- pointer to a texture
*/
void Polygon::setTexture(Texture *texture)
{
	_texture = texture;
}

/*
	Set Type
	- sets the polygon type
	@param type
		- the polygon type
*/
void Polygon::setType(const POLYGON_TYPE type)
{
	_type = type;
}

// VERTEX

//Default constructor
Vertex::Vertex()
{
	//Set position to origin
	_position.x = 0.f;
	_position.y = 0.f;
	_position.z = 0.f;

	//Null out references
	_texturePosition = 0;
	_color = 0;

	//Not referenced by default
	_referenced = false;
}

Vertex::Vertex(const Vector3f position)
{
	Vertex();
	setPosition(position);
}

Vertex::Vertex(const Vector3f position, const Vector2i texturePosition)
{
	Vertex();
	setPosition(position);
	setTexturePosition(texturePosition);
}

Vertex::Vertex(const Vector3f position, const ColorRGB color)
{
	Vertex();
	setPosition(position);
	setColor(color);
}

Vertex::Vertex(const Vector3f position, const Vector2i texturePosition, const ColorRGB color)
{
	Vertex();
	setPosition(position);
	setTexturePosition(texturePosition);
	setColor(color);
}

//Deconstructor
Vertex::~Vertex()
{
	/*
		If any data is pointed at for
		textures or colors. We delete
		the data pointed at.
	*/
	if (_texturePosition)
	{
		delete _texturePosition;
	}
	if (_color)
	{
		delete _color;
	}
}

// Returns the state of calculated
const bool Vertex::calculated()
{
	return _calculated;
}

// Returns the raw position data for this vertex
const Vector3f Vertex::position()
{
	return Vector3f(_position);
}

// If true, this vertex will be preserved on deleting parent polygons
const bool Vertex::referenced()
{
	return _referenced;
}

// Returns the calculated render position for this vertex
const Vector3f Vertex::renderPosition()
{
	return Vector3f(_renderPosition);
}

// Returns a constant copy of the texture position data
const Vector2i Vertex::texturePosition()
{
	//Null ref check
	if (_texturePosition)
	{
		return Vector2i(*_texturePosition);
	}
	else
	{
		//No pointed data, give [0, 0]
		return Vector2i(0, 0);
	}
}

// Returns a constant copy of the color data
const ColorRGB Vertex::color()
{
	//Null ref check
	if (_color)
	{
		return ColorRGB(*_color);
	}
	else
	{
		//No pointed data, give black [0, 0, 0]
		return ColorRGB(0, 0, 0);
	}
}

// Sets the calculated variable
void Vertex::setCalculated(const bool calculated)
{
	_calculated = calculated;
}

/*
	Sets this vertex's object-space position
*/
void Vertex::setPosition(const Vector3f position)
{
	//Store the coordinate data
	_position.x = position.x;
	_position.y = position.y;
	_position.z = position.z;

	//Now we set the render position to this position
	setRenderPosition(position);
}

// Sets value of referenced
void Vertex::setReferenced(const bool referenced)
{
	_referenced = referenced;
}

/*
	Sets the render position
*/
void Vertex::setRenderPosition(const Vector3f renderPosition)
{
	//Store the data
	_renderPosition.x = renderPosition.x;
	_renderPosition.y = renderPosition.y;
	_renderPosition.z = renderPosition.z;
}

/*
	Sets the texture coordinate for this vertex
	- This will overwrite the previous texture coordinate
	- If color is set, it will be removed
*/
void Vertex::setTexturePosition(const Vector2i texturePosition)
{
	//Check if already pointing at something
	if (_texturePosition)
	{
		delete _texturePosition;
	}

	//Allocate memory and copy the constant data
	_texturePosition = new Vector2i(texturePosition.x, texturePosition.y);
}

/*
	Sets the color data for this vertex
	- This will overwrite the previous color data
	- If texturePosition is set, it will be removed

*/
void Vertex::setColor(const ColorRGB color)
{
	//Check if already pointing at something
	if (_color)
	{
		delete _color;
	}

	//Allocate memory and copy the constant data
	_color = new ColorRGB(color.r, color.g, color.b);
}

/*
	Texture Position Set
	@output true
		- if texturePosition has been set
	@outout false
		- if texturePosition has not been set
*/
bool Vertex::texturePositionSet()
{
	return (_texturePosition);
}

/*
	Color Set
	@output true
	- if color has been set
	@outout false
	- if color has not been set
*/
bool Vertex::colorSet()
{
	return (_color);
}


// RENDERER

//Default constructor
Renderer::Renderer()
{
	//No polygons yet
	polygons = 0;
	polygonCount = 0;
	//No clear texture yet
	clearTexture = 0;
}

//Deconstructor
Renderer::~Renderer()
{
	//Remove the polygon ref and position arrays
	if (polygons)
	{
		delete polygons;
	}
}

/*
	Add Polygon
	- adds a polygon to the renderer
	@param polygon
		- The reference to the polygon to render
*/
void Renderer::addPolygon(Polygon *polygon)
{
	//If no polygons yet
	if (!polygons)
	{
		polygonCount = 1;
		polygons = new Polygon*[polygonCount];
		polygons[0] = polygon;
		return;
	}

	//Temp array to store the existing data
	Polygon **tempPolygons = polygons;

	//Create the reference arrays of data with new size
	polygonCount++;
	polygons = new Polygon*[polygonCount];

	//Copy over the old reference data
	for (int i = 0; i < polygonCount - 1; i++)
	{
		polygons[i] = tempPolygons[i];
	}

	//Store the supplied data
	polygons[polygonCount - 1] = polygon;

	//Delete the temporary array
	delete tempPolygons;
}

/*
	Clean
	- clears all data
*/
void Renderer::clean()
{
	//Polygons
	if (polygons)
	{
		delete polygons;
	}
	polygons = 0;

	//Reset polygon count
	polygonCount = 0;
}

/*
	Render
	- completes the render of a scene
*/
void Renderer::render()
{
	//Reference variables
	Polygon *polygon;

	//Loop counters
	int i, j;

	//Track the number of rendered vertices - for keeping us under the max
	int vertexCount = 0;

	for (i = 0; i < polygonCount; i++)
	{
		//Get reference to this polygon
		polygon = polygons[i];

		//If there is a texture, make sure texturing is enabled and then bind it
		if (polygon->texture())
		{
			glBindTexture(0, polygon->texture()->id());
		}
		else
		{
			if (clearTexture)
			{
				glBindTexture(0, clearTexture->id());
			}
		}

		//Start drawing this polygon
		glColor3f(1, 1, 1);
		glBegin(polygon->type());

		//Go through vertices
		for (j = 0; j < polygon->vertexCount(); j++)
		{
			//Check vertex limit
			if (vertexCount >= MAX_VERTICES)
			{
				//End the draw
				glEnd();

				// Max vertices reached
				return;
			}

			//Render this vertex
			RenderVertex(polygon, polygon->vertices()[j]);
			vertexCount++;
		}

		//Do we render the back too?
		if (polygon->backRender())
		{
			for (j =polygon->vertexCount() - 1; j >= 0; j--)
			{
				//Check vertex limit
				if (vertexCount >= MAX_VERTICES)
				{
					//End the draw
					glEnd();

					// Max vertices reached
					return;
				}

				//Render this vertex
				RenderVertex(polygon, polygon->vertices()[j]);
				vertexCount++;
			}
		}
		glEnd();
	}
}

//Sets the clear texture
void Renderer::setClearTexture(Texture *texture)
{
	clearTexture = texture;
}

//Renders the vertex
void RenderVertex(Polygon *polygon, Vertex *vertex)
{
	//Set texture coord
	if (polygon->texture() && vertex->texturePositionSet())
	{
		//Set texture coordinates
		glTexCoord2t16(
				inttot16(vertex->texturePosition().x),
				inttot16(vertex->texturePosition().y));
	}
	else
	{
		glTexCoord2t16(
			inttot16(0),
			inttot16(0));
	}

	//Set vertex color
	if (vertex->colorSet())
	{
		glColor3b(
			vertex->color().r,
			vertex->color().g,
			vertex->color().b);
	}
	
	//Render the vertex
	glVertex3v16(
		floattov16(vertex->renderPosition().x),
		floattov16(vertex->renderPosition().y),
		floattov16(vertex->renderPosition().z));
}

// ROOM

// Default constructor
Room::Room()
{
	// Null out arrays
	_mainObject = 0;
	_portals = 0;

	// No portals yet
	_portalCount = 0;
}

// Deconstructor
Room::~Room()
{
	// Delete portals
	if (_portalCount > 0 && _portals)
	{
		for (int i = 0; i < _portalCount; i++)
			if (_portals[i])
				delete _portals[i];
		delete _portals;
	}
}

/*
	Add Portal
	- Creates a new portal connecting this
		room to another one
	@param connected
		- The room we are connecting to
	@param container
		- The object containing bounding
			data for this portal
*/
void Room::addPortal(Portal *portal)
{
	// No portals yet
	if (_portalCount == 0)
	{
		if (_portals)
			delete _portals;
		_portals = new Portal*[++_portalCount];
		_portals[0] = portal;
		return;
	}

	// Resize array and copy data
	Portal **temp_portals = _portals;
	_portals = new Portal*[_portalCount + 1];

	// Copy old data to newly sized array
	for (int i = 0; i < _portalCount; i++)
	{
		_portals[i] = temp_portals[i];
	}

	// Add new portal and increment counter
	_portals[_portalCount++] = portal;

	// Clean up temp array
	delete temp_portals;
}

// Gets the number of portals
const int Room::portalCount()
{
	return _portalCount;
}

// Gets a reference to the portals array
Portal **Room::portals()
{
	return _portals;
}

// Gets value of rendered
const bool Room::rendered()
{
	return _rendered;
}

// Gets a reference to the main object
Object *Room::mainObject()
{
	return _mainObject;
}

/*
	Set Main Object
	- Sets the render object for this room
	- Best practice is to keep polygon data
		empty for this object and fill only
		the children with objects contained
		within this room
	- Portal objects do not have to be here
		but including them will not make a
		difference
*/
void Room::setMainObject(Object *mainObject)
{
	_mainObject = mainObject;
}

// Sets the value of the rendered variable
void Room::setRendered(const bool rendered)
{
	_rendered = rendered;
}

// PORTAL

// Default constructor
Portal::Portal()
{
	// Null out data
	_connected = 0;
	_container = 0;
}

// Deconstructor
Portal::~Portal()
{
	// We only hold reference data, no deconstructing
}

// Get the connected room
Room *Portal::connected()
{
	return _connected;
}

// Get the container object
Object *Portal::container()
{
	return _container;
}

// Sets the connected room
void Portal::setConnected(Room *connected)
{
	_connected = connected;
}

// Sets the container object
void Portal::setContainer(Object *container)
{
	_container = container;
}