#include "../include/dnds_3d_textures.h"

// TEXTURE MANAGER

//Public-facing accessor to singleton
TextureManager *TextureManager::getTextureManager()
{
	//If doesn't exist, create it
	if (!s_instance)
		s_instance = new TextureManager();
	//Return the singleton instance
	return s_instance;
}

//Constructor
TextureManager::TextureManager()
{
	//Clear all data
	textures = 0;
	textureCount = 0;
	textureIDs = 0;
}

//Deconstructor
TextureManager::~TextureManager()
{
	//Deletes all texture data
	if (textures)
	{
		//Delete each texture individually
		for (int i = 0; i < textureCount; i++)
			delete textures[i];
		//Delete the reference array
		delete textures;
	}

	//Clears id data
	if (textureIDs)
	{
		delete textureIDs;
	}
}

void TextureManager::addTexture(Texture *texture)
{
	//First texture added
	if (!textures)
	{
		delete textures;
		textureCount = 1;
		textures = new Texture*[textureCount];
		textures[0] = texture;
		return;
	}

	//Temp texture holder
	Texture **temp_textures = textures;

	//New array
	textures = new Texture*[++textureCount];

	//Copy back old data
	for (int i = 0; i < textureCount - 1; i++)
		textures[i] = temp_textures[i];

	//Delete temp array and store new texture
	delete temp_textures;
	textures[textureCount - 1] = texture;
}

/*
	Commits all texture data in the manager to memory
	- Returns true if all succeeded, false if any fail
*/
bool TextureManager::commitTextures()
{
	//Memory allocated - in bytes
	int allocated = 0;

	//Get the return array of ints ready for IDs
	if (textureIDs)
		delete textureIDs;
	textureIDs = new int[textureCount];

	//Generate the texture space
	bool result = (glGenTextures(textureCount, textureIDs) == 1);

	//Map the textures one by one (Sets their ids)
	for (int i = 0; i < textureCount; i++)
	{
		if (textures[i]->data())
		{
			allocated += textures[i]->length();
			result = textures[i]->map(&textureIDs[i]) && result;
		}
	}

	return result;
}

// TEXTURE

//Constructor
Texture::Texture()
{
	//Default values
	_width = TEXTURE_SIZE_64;
	_height = TEXTURE_SIZE_64;
	_type = GL_RGB;
	//Null out data
	_id = 0;
	_data = 0;
}

//Return the int value of the referenced id
const int Texture::id()
{
	return (*_id);
}

//Return pointer to data
u8* Texture::data()
{
	return _data;
}

//Return our bitmap length
const int Texture::length()
{
	//Return _length
	return _length;
}

//Maps the texture to the id and into VRAM
bool Texture::map(int *id)
{
	//Nullchecks
	if (!_data || !id)
		return false;

	//Store the id reference
	_id = id;

	//If success, returns true
	glBindTexture(0, *id);

	bool result = glTexImage2D(
		0,
		0,
		_type,
		_width,
		_height,
		0,
		TEXGEN_TEXCOORD,
		_data) == 1;

	return result;
}

//Sets the location of the texture data
void Texture::setData(u8 *data)
{
	_data = data;
}

//Sets the length data
void Texture::setLength(int length)
{
	_length = length;
}

//Set dimensions
void Texture::setSize(GL_TEXTURE_SIZE_ENUM width, GL_TEXTURE_SIZE_ENUM height)
{
	_width = width;
	_height = height;
}

//Set texture colour type
void Texture::setType(GL_TEXTURE_TYPE_ENUM type)
{
	_type = type;
}