#include "../include/game.h"

// Default constructor
Game::Game()
{
	// Null out oam table
	oam = 0;
}

void Game::init()
{
	// Create the oam table
	oam = new OAMTable();
	initOAMTable(oam);
	initSprites();
}

void Game::initSprites()
{
	// Var for tracking tile indices
	int nextAvailableTileIndex = 0;
	// Current id
	int id;
	// References to the current sprite data
	SpriteInfo *info;
	SpriteEntry *entry;

	// TITLE
	id = TITLE_OAM_ID;
	info = getSpriteInfo(id);
	entry = getSpriteEntry(id);
	// Info
	info->oamID = id;
	info->width = 64;
	info->height = 64;
	info->angle = 0;
	info->entry = entry;
	// OAM attributes
	// 0 - affine
	entry->isRotateScale = true;
	entry->isSizeDouble = true;
	entry->blendMode = OBJMODE_NORMAL;
	entry->isMosaic = false;
	entry->colorMode = OBJCOLOR_16;
	entry->shape = OBJSHAPE_SQUARE;
	// 1 - transforms
	entry->x = 256;
	entry->y = 32;
	entry->size = OBJSIZE_64;
	// 2 - memory data
	entry->gfxIndex = nextAvailableTileIndex;
	nextAvailableTileIndex += titleTilesLen / BYTES_PER_16_COLOR_TILE;
	entry->priority = OBJPRIORITY_0;
	entry->palette = info->oamID;
	// Scale to double
	scaleSprite(&oam->matrixBuffer[id], 2, 2);

	// PAUSE
	id = PAUSE_OAM_ID;
	info = getSpriteInfo(id);
	entry = getSpriteEntry(id);
	// Info
	info->oamID = id;
	info->width = 64;
	info->height = 64;
	info->angle = 0;
	info->entry = entry;
	// OAM attributes
	// 0 - affine
	entry->isRotateScale = true;
	entry->isSizeDouble = true;
	entry->blendMode = OBJMODE_NORMAL;
	entry->isMosaic = false;
	entry->colorMode = OBJCOLOR_16;
	entry->shape = OBJSHAPE_SQUARE;
	// 1 - transforms
	entry->x = 64;
	entry->y = -96;
	entry->size = OBJSIZE_64;
	// 2 - memory data
	entry->gfxIndex = nextAvailableTileIndex;
	nextAvailableTileIndex += pauseTilesLen / BYTES_PER_16_COLOR_TILE;
	entry->priority = OBJPRIORITY_0;
	entry->palette = info->oamID;
	// Scale to double
	scaleSprite(&oam->matrixBuffer[id], 2, 2);

	// COPY PALETTES
	// Title
	dmaCopyHalfWords(SPRITE_DMA_CHANNEL,
		titlePal,
		&SPRITE_PALETTE_SUB[TITLE_OAM_ID * COLORS_PER_PALETTE],
		titlePalLen);
	// Pause
	dmaCopyHalfWords(SPRITE_DMA_CHANNEL,
		pausePal,
		&SPRITE_PALETTE_SUB[PAUSE_OAM_ID * COLORS_PER_PALETTE],
		pausePalLen);

	// COPY TILES
	// Title
	dmaCopyHalfWords(SPRITE_DMA_CHANNEL,
		titleTiles,
		&SPRITE_GFX_SUB[getSpriteEntry(TITLE_OAM_ID)->gfxIndex * OFFSET_MULTIPLIER],
		titleTilesLen);
	// Pause
	dmaCopyHalfWords(SPRITE_DMA_CHANNEL,
		pauseTiles,
		&SPRITE_GFX_SUB[getSpriteEntry(PAUSE_OAM_ID)->gfxIndex * OFFSET_MULTIPLIER],
		pauseTilesLen);
}

Game *Game::getGame()
{
	if (!s_instance)
		s_instance = new Game();
	return s_instance;
}

OAMTable *Game::getOAM()
{
	return oam;
}

void Game::update()
{
	updateOAM(oam);
}

SpriteInfo *Game::getSpriteInfo(const u16 id)
{
	return &spriteInfo[id];
}

SpriteEntry *Game::getSpriteEntry(const u16 oamId)
{
	return &oam->oamBuffer[oamId];
}
