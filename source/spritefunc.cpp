#include "../include/spritefunc.h"

void updateOAM(OAMTable *oam)
{
	DC_FlushAll();
	dmaCopyHalfWords(SPRITE_DMA_CHANNEL,
		oam->oamBuffer,
		OAM_SUB,
		SPRITE_COUNT * sizeof(SpriteEntry));
}

void initOAMTable(OAMTable *oam)
{
	//Clear attributes of sprites
	for (int i = 0; i < SPRITE_COUNT; i++)
	{
		oam->oamBuffer[i].attribute[0] = ATTR0_DISABLED;
		oam->oamBuffer[i].attribute[1] = 0;
		oam->oamBuffer[i].attribute[2] = 0;
	}
	//Init matrix to identity matrix
	for (int i = 0; i < MATRIX_COUNT; i++)
	{
		oam->matrixBuffer[i].hdx = 1 << 8;
		oam->matrixBuffer[i].hdy = 0;
		oam->matrixBuffer[i].vdx = 0;
		oam->matrixBuffer[i].vdy = 1 << 8;
	}
	updateOAM(oam);
}

void moveSprite(SpriteEntry *spriteEntry, u16 x, u16 y)
{
	spriteEntry->x = x;
	spriteEntry->y = y;
}

void scaleSprite(SpriteRotation *spriteRotation, int sx, int sy)
{
	spriteRotation->hdx = sx << 6;
	spriteRotation->vdy = sy << 6;
}

void rotateSprite(SpriteRotation *spriteRotation, int angle)
{
	s16 s = sinLerp(angle) >> 4;
	s16 c = cosLerp(angle) >> 4;

	spriteRotation->hdx = c;
	spriteRotation->hdy = s;
	spriteRotation->vdx = -s;
	spriteRotation->vdy = c;
}

void setPriority(SpriteEntry *spriteEntry, ObjPriority priority)
{
	spriteEntry->priority = priority;
}

void setSpriteVisibility(SpriteEntry *spriteEntry, bool hidden, bool affine, bool doubleBound)
{
	if (hidden)
	{
		//Make sprite invisible
		spriteEntry->isRotateScale = false;
		spriteEntry->isHidden = true;
	}
	else
	{
		//Make sprite visible
		if (affine)
		{
			spriteEntry->isRotateScale = true;
			spriteEntry->isSizeDouble = doubleBound;
		}
		else
		{
			spriteEntry->isHidden = false;
		}
	}
}