#include "../include/dnds_3d_input.h"

/*
	Default constructor
*/
InputManager::InputManager()
{
	// Clear values
	_keysHeld = 0;
	_keysDown = 0;

	// Null reference to update function
	_updateFunction = 0;
}

/*
	Deconstructor
*/
InputManager::~InputManager()
{
	// Nothing to do here
}

/*
	Convert Button to libnds Button types
*/
uint32 InputManager::keyFromButton(const Button button)
{
	switch (button)
	{
	case A:
		return KEY_A;
	case B:
		return KEY_B;
	case X:
		return KEY_X;
	case Y:
		return KEY_Y;
	case L:
		return KEY_L;
	case R:
		return KEY_R;
	case UP:
		return KEY_UP;
	case DOWN:
		return KEY_DOWN;
	case LEFT:
		return KEY_LEFT;
	case RIGHT:
		return KEY_RIGHT;
	case SELECT:
		return KEY_SELECT;
	case START:
		return KEY_START;
	case TOUCHPAD:
		return KEY_TOUCH;
	case LID:
		return KEY_LID;
	default:
		return 0;
	}
}

/*
	Gets the singleton reference
*/
InputManager *InputManager::getInputManager()
{
	//If doesn't exist, create it
	if (!s_instance)
		s_instance = new InputManager();
	//Return the singleton instance
	return s_instance;
}

/*
	Check down state of button
*/
const bool InputManager::keyPressed(const Button button)
{
	return _keysDown & keyFromButton(button);
}

/*
	Check held state of button
*/
const bool InputManager::keyHeld(const Button button)
{
	return _keysHeld & keyFromButton(button);
}

/*
	Check up state of button
*/
const bool InputManager::keyUp(const Button button)
{
	return _keysUp & keyFromButton(button);
}

/*
	Check if screen is touched
*/
const bool InputManager::screenTouched()
{
	return _keysDown & KEY_TOUCH;
}

/*
	Check if screen is held
*/
const bool InputManager::screenDragged()
{
	return _keysHeld & KEY_TOUCH;
}

// Did the lid just close
const bool InputManager::lidClosed()
{
	return _keysDown & KEY_LID;
}

// Did the lid just open
const bool InputManager::lidOpened()
{
	return _keysUp & KEY_LID;
}

/*
	Gets the currently recorded value of touch position
*/
const touchPosition InputManager::getTouchPosition()
{
	return _touchPosition;
}

/*
	Scans state of keys and updates registers
*/
void InputManager::update()
{
	// Query state
	scanKeys();

	// Update our values
	_keysHeld = keysHeld();
	_keysUp = keysUp();
	_keysDown = keysDown();

	// Get the touch position
	touchRead(&_touchPosition);

	// If an update function has been set, call it
	if(_updateFunction)
		(*_updateFunction)(this);
}

// Sets the function to call at the end of each update() call
void InputManager::setUpdateFunction(void(*updateFunction)(InputManager*))
{
	_updateFunction = updateFunction;
}
