#include "../include/dnds_3d.h"

/*
	Indoor Renderer
	- Sets up renderer with indoor
		room/portal optimizations
*/
static void indoorRenderer(Scene *scene, BaseObject *object);

/*
	Default Renderer
	- Performs standard frustum
		optimizations
*/
static void defaultRenderer(Scene *scene, BaseObject *object);

/*
	Add Render Object
	- processes the object for rendering
	@param scene
		- the reference to the scene
	@param object
		- the reference to the object
	@paramm forceRender
		- ignore culling on this object directly
			(Does not carry to recursive calls)
*/
static void addRenderObject(Scene *scene, BaseObject *object, bool forceRender = false);

/*
	Object In Frustum
	- Checks if an object is in the frustum
	@param object
	@param scene
*/
static bool objectInFrustum(BaseObject *object, Scene *scene);

// Function used to initialize the engine
void initEngine()
{
	// We are in 3D mode - main renderer
	videoSetMode(MODE_0_3D);

	//Initialize openGL
	glInit();

	// Our enables
	glEnable(GL_TEXTURE_2D | GL_ANTIALIAS); // POLY_FORMAT_LIGHT0 | POLY_FORMAT_LIGHT1);

	//Black background color
	glClearColor(0, 0, 0, 31);
	glClearPolyID(63);
	glClearDepth(0x7FFF);

	//Set to the DS screensize
	glViewport(0, 0, 255, 191);

	//Set up VRAM for texturing - map as required from LCD to other bank types
	// Please see: https://mtheall.com/banks.html
	vramSetBankA(VRAM_A_TEXTURE);
	vramSetBankB(VRAM_B_TEXTURE);
	vramSetBankC(VRAM_C_TEXTURE);
	vramSetBankD(VRAM_D_TEXTURE);
	vramSetBankE(VRAM_E_LCD);
	vramSetBankF(VRAM_F_LCD);

	//Material Initialization
	//TODO allow customization
	glMaterialf(GL_AMBIENT, RGB15(16, 16, 16));
	glMaterialf(GL_DIFFUSE, RGB15(16, 16, 16));
	glMaterialf(GL_SPECULAR, BIT(15) | RGB15(8, 8, 8));
	glMaterialf(GL_EMISSION, RGB15(16, 16, 16));
	glMaterialShinyness();

	//Culling
	glPolyFmt(POLY_ALPHA(31) | POLY_CULL_BACK);
}


//Sets the clear color
void setClearColor(ColorRGB color)
{
	glClearColor(color.r / 8, color.g / 8, color.b / 8, 31);
}

//Function used to initialize the camera
void initCamera(bool perspective)
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (perspective)
	{
		// Perspective
		gluPerspective(70, 256.0 / 192.0, 0.01, 100);
	}
	else
	{
		//Ortho
		glOrtho(-0.5, 0.5, -0.5, 0.5, -10, 10);
	}
}

//Function used to set up a scene render
void renderStart(Scene *scene)
{
	//Start at the base object and recursively add data to the renderer
	BaseObject *baseObject = scene->baseObject();

	//Reference check
	if (!baseObject)
	{
		//TODO output error
		return;
	}

	//Clear the scene's renderer
	scene->renderer()->clean();

	//Use the optimal render function for the render type
	if (scene->room())
		indoorRenderer(scene, baseObject);
	else
		defaultRenderer(scene, baseObject);

	//If there is an update function, we will call it using the delta time
	if (scene->hasUpdateFunction())
	{
		float deltaTime = scene->frameCount();
		if (deltaTime >= scene->frameTime())
		{
			// Call update
			scene->update(deltaTime - scene->frameTime());

			// Reset the frame counter
			scene->resetFrameCount();
		}
	}

	//Recalc the bounds - have to do this for culling to work
	scene->baseObject()->calculateVertices();
}

//Function used to complete the actual render
void renderEnd(Scene *scene)
{
	//Set up normals
	glNormal(NORMAL_PACK(0, 0, 0));

	//Use the renderer to render the scene
	scene->renderer()->render();

	// Wait until the geometry engine is not busy
	while (GFX_STATUS & (1 << 27));

	// Flush to screen
	glFlush(0);

	//Vertical blanking
	swiWaitForVBlank();

	//Incrememnt counter if updating is set
	if (scene->hasUpdateFunction())
		scene->countFrame();
}

/*
	Indoor Renderer
	- Sets up renderer with indoor
		room/portal optimizations
*/
static void indoorRenderer(Scene *scene, BaseObject *object)
{
	// Render the frustum if visible
	if (scene->frustumObject() && scene->frustumObject()->visible())
		addRenderObject(scene, scene->frustumObject(), true);

	// Reference to the room we are in
	Room *room = scene->room();

	// Loop through all room portals to reset their render status
	for (int i = 0; i < room->portalCount(); i++)
	{
		room->portals()[i]->connected()->setRendered(false);
	}

	// First add the room to the renderer and set it to rendered
	addRenderObject(scene, room->mainObject(), true);
	room->setRendered(true);

	// Loop through all room portals
	for (int i = 0; i < room->portalCount(); i++)
	{
		// Has the room connected to by this portal already been rendered?
		if (!room->portals()[i]->connected()->rendered())
		{
			// Is the portal in the frustum?
			if (objectInFrustum(room->portals()[i]->container(), scene))
			{
				// Render the connected room and set it to rendered
				addRenderObject(scene, room->portals()[i]->connected()->mainObject(), true);
				room->portals()[i]->connected()->setRendered(true);
			}
		}
	}
}

/*
	Default Renderer
	- Performs standard frustum
		optimizations
*/
static void defaultRenderer(Scene *scene, BaseObject *object)
{
	// Just add the base scene object to the recursive function
	addRenderObject(scene, object);
}

/*
	Add Render Object
	- processes the object for rendering
	@param scene
		- the reference to the rendering scene
	@param object
		- the reference to the object
	@paramm forceRender
		- ignore culling on this object directly
		(Does not carry to recursive calls)
*/
static void addRenderObject(Scene *scene, BaseObject *object, bool forceRender)
{
	// If this object is invisible, forget it
	if (!object->visible())
		return;

	// If the object is outside the viewing frustum, forget it
	if (!forceRender && !objectInFrustum(object, scene))
		return;

	// Add all polygon data to the renderer
	for (int i = 0; i < object->polygonCount(); i++)
	{
		scene->renderer()->addPolygon(object->polygons()[i]);
	}

	// Recursively check all child objects
	for (int i = 0; i < object->childCount(); i++)
	{
		addRenderObject(scene, object->children()[i], forceRender);
	}
}

/*
	Object In Frustum
	- Checks if an object is in the frustum
	@param object
	@param scene
*/
static bool objectInFrustum(BaseObject *object, Scene *scene)
{
	/*
		Check if this object's bounding box is within the view frustum

		Method:
			- Check if both bounds in an axis are less than the low bound of
				the frustum in that axis
			- If they are both less, skip this object (And all children)
			- If they aren't check if both bounds are greater that the high
				bound of the frustum in that axis
			- If they are both greater, skip this object (And all children)
			- Repeat for X, Y, and Z axes. If all pass, add polygons to renderer
				and then recursively call AddObject on all children
	*/
	
	if (scene->frustumObject())
	{
		// Check left
		if (object->bound(0) < scene->frustumBound(0) &&
			object->bound(1) < scene->frustumBound(0))
			return false;
		// Check right
		if (object->bound(0) > scene->frustumBound(1) &&
			object->bound(1) > scene->frustumBound(1))
			return false;
		// Check bottom
		if (object->bound(2) < scene->frustumBound(2) &&
			object->bound(3) < scene->frustumBound(2))
			return false;
		// Check top
		if (object->bound(2) > scene->frustumBound(3) &&
			object->bound(3) > scene->frustumBound(3))
			return false;
		// Check far
		if (object->bound(4) < scene->frustumBound(4) &&
			object->bound(5) < scene->frustumBound(4))
			return false;
		// Check near
		if (object->bound(4) > scene->frustumBound(5) &&
			object->bound(5) > scene->frustumBound(5))
			return false;
	}

	return true;
}