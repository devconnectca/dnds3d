#include "../include/dnds_3d_anim.h"

// ANIMATION CLASS

// Default constructor
Animation::Animation()
{
	// Null out references
	_keyframes = 0;
	_chained = 0;

	// Default keyframe info
	_keyframeCount = 0;
	_currentKeyframe = 0;

	// Looping defaults to off
	_looping = false;

	// Not playing
	_playing = false;
}

// Deconstructor
Animation::~Animation()
{
	// Delete keyframes
	for (int i = 0; i < _keyframeCount; i++)
	{
		delete _keyframes[i];
	}
	delete _keyframes;
}

// Inserts a keyframe into the animation
void Animation::addKeyframe(Keyframe *keyframe)
{
	if (!_keyframes || _keyframeCount == 0)
	{
		if (_keyframes)
			delete _keyframes;
		_keyframeCount = 1;
		_keyframes = new Keyframe*[1];
		_keyframes[0] = keyframe;
		return;
	}

	Keyframe **temp_keyframes = _keyframes;
	_keyframes = new Keyframe*[_keyframeCount + 1];

	for (int i = 0; i < _keyframeCount; i++)
	{
		_keyframes[i] = temp_keyframes[i];
	}

	_keyframes[_keyframeCount++] = keyframe;

	delete temp_keyframes;
}

// Starts/Resumes the animation
void Animation::play()
{
	if (!_playing)
	{
		_playing = true;
	}
}

// Pauses the animation - does nothing if already paused
void Animation::pause()
{
	if (_playing)
		_playing = false;
}

// Stops the animation - resets it to initial state
void Animation::stop()
{
	// First pause it
	pause();

	// Reset current frame count
	_currentKeyframe = 0;

	//Interpolate points for current frame
	if (_keyframeCount > 0)
	{
		_keyframes[_currentKeyframe]->setFrameTime(0);
		_keyframes[_currentKeyframe]->calculateIncrements();
	}
}

/*
	Update Animation
	- if playing, will update the current keyframe
	@param deltaTime
		- The time since last update was called. If
			0 or less is passed, will assume standard
			frame time of 1
*/
void Animation::update()
{
	// Are we playing?
	if (!_playing)
		return;

	// Update current keyframe
	if (_keyframeCount > 0 && _keyframes[_currentKeyframe])
	{

		if (_keyframes[_currentKeyframe]->finished())
		{
			// Run end function
			_keyframes[_currentKeyframe]->runEndFunction();

			// Next frame
			_currentKeyframe++;

			// Last frame?
			if (_currentKeyframe >= _keyframeCount)
			{
				
				// Done this animation (reset it)
				stop();

				// Check if looping, then loop, if not, check if chained
				if (_looping)
				{
					play();
				}
				else if (_chained)
				{
					_chained->play();
				}
			}
			else
			{
				_keyframes[_currentKeyframe]->calculateIncrements();
				_keyframes[_currentKeyframe]->setFrameTime(0);
			}
		}
		else 
		{
			// Update current keyframe
			_keyframes[_currentKeyframe]->update();
		}
	}
}

// Are we playing?
const bool Animation::playing()
{
	return _playing;
}

/*
	Sets the chained aninmation
		- A chained animation will have
			start called on it as soon
			as this animation finishes
		@param chained
			- Pointer to animation to run
				after this one finished
			- Pass 0 (NULL) if you wish
				to not have a chained
				animation [DEFAULT]
*/
void Animation::setChained(Animation *chained)
{
	_chained = chained;
}

// Does the animation loop?
void Animation::setLooping(const bool looping)
{
	_looping = looping;
}


// KEYFRAME CLASS

// Calculates the increments
void Keyframe::calculateIncrements()
{
	if (_vertexCount == 0)
		return;
	
	// Clear out incrememnts
	if (_increments)
		delete _increments;
	_increments = new Vector3f[_vertexCount];

	// Linear interpolation of keyframe data
	for (int i = 0; i < _vertexCount; i++)
	{
		_increments[i].x = (_targets[i].x - _vertices[i]->position().x) / (float)_frameLength;
		_increments[i].y = (_targets[i].y - _vertices[i]->position().y) / (float)_frameLength;
		_increments[i].z = (_targets[i].z - _vertices[i]->position().z) / (float)_frameLength;
	}
}

// Default constructor
Keyframe::Keyframe()
{
	// Null out arrays
	_vertices = 0;
	_increments = 0;
	_targets = 0;

	// No referenced vertices yet
	_vertexCount = 0;

	// Default vals for keyframe data
	_frameLength = 10;
	_frameTime = 0;
	_endFunction = 0;
}

// Constructor that sets frame length
Keyframe::Keyframe(const int frameLength)
{
	// Call default constructor
	Keyframe();

	// Set frame length
	_frameLength = frameLength;
}

// Deconstructor
Keyframe::~Keyframe()
{
	// Clean up arrays
	if (_vertices)
		delete _vertices;
	if (_increments)
		delete _increments;
	if (_targets)
		delete _targets;
}

// Adds a vertex to animate with a target position (In object space)
void Keyframe::addVertex(Vertex *vertex, const Vector3f target)
{
	// No vertices yet
	if (_vertexCount == 0)
	{
		// Populate vertices
		if (_vertices)
			delete _vertices;
		_vertexCount = 1;
		_vertices = new Vertex*[_vertexCount];
		_vertices[0] = vertex;

		// Populate targets
		if (_targets)
			delete _targets;
		_targets = new Vector3f[_vertexCount];
		_targets[0] = target;
		
		// Calculate the increments again
		calculateIncrements();

		return;
	}

	// Temp arrays
	Vertex **temp_vertices = _vertices;
	Vector3f *temp_targets = _targets;

	// Clear out incrememnts
	if (_increments)
		delete _increments;
	_increments = new Vector3f[_vertexCount + 1];

	// Reformat arrays
	_vertices = new Vertex*[_vertexCount + 1];
	_targets = new Vector3f[_vertexCount + 1];

	// Copy old data over
	for (int i = 0; i < _vertexCount; i++)
	{
		_vertices[i] = temp_vertices[i];
		_targets[i] = temp_targets[i];
	}

	// Update data and copy new data in
	_vertexCount++;
	_vertices[_vertexCount - 1] = vertex;
	_targets[_vertexCount - 1] = target;

	// Delete the temp vertex array (Not targets)
	delete temp_vertices;

	// Calculate the increments again
	calculateIncrements();
}

// Updates the keyframe
void Keyframe::update()
{
	// Check if this keyframe is finished
	bool done = finished();
	if (done)
		return;

	// Not finished, perform the update
	_frameTime++;

	// Move each vertex by its increment
	for (int i = 0; i < _vertexCount; i++)
	{
		// If we are finished now, set vertex positions to target ones, otherwise, increment
		if (done)
		{
			_vertices[i]->setPosition(_targets[i]);
		}
		else
		{
			// Get current vertex position
			position = _vertices[i]->position();

			// Add the new value
			position.x += _increments[i].x;
			position.y += _increments[i].y;
			position.z += _increments[i].z;

			// Update the position
			_vertices[i]->setPosition(position);
		}
	}
}

// Run the end function
void Keyframe::runEndFunction()
{
	(*_endFunction)();
}

/*
	Is this keyframe finished running
	@output true: keyframe is finished
			false: keyframe is not finished
*/
const bool Keyframe::finished()
{
	return (_frameTime == _frameLength);
}

// Sets the end function
void Keyframe::setEndFunction(void(*endFunction)())
{
	_endFunction = endFunction;
}

	// Sets number of updates required for this keyframe
void Keyframe::setFrameLength(const int frameLength)
{
	_frameLength = frameLength;

	// Incrememnts are based on frame length
	calculateIncrements();
}

// Sets the current time in the animation - bounded by 0 and frameLength
void Keyframe::setFrameTime(const int frameTime)
{
	if (frameTime < 0 || frameTime > _frameLength)
		return;

	_frameTime = frameTime;
}
