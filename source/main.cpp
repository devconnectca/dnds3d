//Include the engine
#include "../include/dnds_3d.h"

// Clear texture header
#include "../build/clear.h"

// Texture headers
#include "../build/background_bot.h"

// 2D bitmap background data
#include "../build/background_top.h"

// Game
#include "../include/game.h"

// Generic stuff
#include <string>
#include <time.h>
#include <vector>

// FUNCTION DECLARATIONS

// Initialization
void initAudio();
void initEnemies();
void initLevel();
void initTextures();

// Player functions
void resetPlayer();
void shootBullet(bool pressed);

// Updating
void handleInput(InputManager *input);
void update2D();
void update3D();
void updateCamera();
void update(float deltaTime);

// Game functions
void togglePause();
void showMessage(std::string message, int duration);
void startGame();
void skipIntro();
void spawnBoss();
void spawnEnemy(bool boss_enemy = false);

//Initialize the texture manager
TextureManager *TextureManager::s_instance = 0;

//Initialize the input manager
InputManager *InputManager::s_instance = 0;

//Initialize the audio manager
AudioManager *AudioManager::s_instance = 0;

//Initialize the game
Game *Game::s_instance = 0;

//The clear texture
Texture tex_clear;

// Our texture list
Texture tex_sky;

// Our scene list
Scene mainScene;

// Our object list
Object ground;
Object player;
Object shadow;
Object sky;
Vertex sky_verts[4];

// Our active scene
Scene *scene = &mainScene;

//Camera variables
Vector3f cam_pos;
Vector2i cam_rot;

// Scrolling image data
int scrollX = 0;
u16 bg_sky;

// Text image data
int textX, textY;
u16 bg_text;
std::string displayText;
int textTime = 0;

// Music volume
const char GAME_VOL = 150;
char musicVol = GAME_VOL;
// Endgame music
bool mainTheme; // Are we playing the main theme still?

// Title card and pause menu
const int TEXT_FLASH_TIME = 20;
int flashTime = TEXT_FLASH_TIME;
bool textOn = false;
int titleScroll = 256;
int pauseScroll = -96;

// Player variables
// position
Vector3f pos;
Vector3i rot;
const Vector3f POS_DEFAULT(-0.75f, 0.5f, 5.f);
// stats
const float MAX_HEALTH = 100.f;
float health = MAX_HEALTH;
int deaths = 0;
// shooting
const int BULLET_COUNT = 15;
const int BULLET_COOLDOWN = 15;
const float BULLET_DAMAGE = 5.f;
int firedBullets = 0;
int bulletTimer = BULLET_COOLDOWN;
Bullet bullets[BULLET_COUNT];
// Level progress
const int LEVEL_SIZE = 2000;
int distanceLeft = LEVEL_SIZE;

// Enemies
const float ENEMY_DAMAGE = 10.f;
const int ENEMY_COUNT = 10;
Enemy enemies[ENEMY_COUNT];
int spawnedEnemies = 0;

// Boss
const float BOSS_HEALTH_MAX = 100.f;
const int MOVE_TIME = 50;
const int DEFAULT_SPAWN_TIME = 100;
const int MIN_SPAWN_TIME = 10;
float boss_health = BOSS_HEALTH_MAX;
Enemy boss;
Vector3f target;
int currentSpawnTime = DEFAULT_SPAWN_TIME;
int spawnTimer = currentSpawnTime;
int moveTimer = MOVE_TIME;

// Our game state
State state = TITLE;

// Sound effects
SoundEffect *sfx_pause;
SoundEffect *sfx_unpause;
SoundEffect *sfx_bullet;
SoundEffect *sfx_die;
SoundEffect *sfx_crash;
SoundEffect *sfx_enemy_collide;
SoundEffect *sfx_enemy_hit;

// Console output
PrintConsole *console;

//Main application entry point
int main()
{
	// Initialize the engine - with subrenderer
	initEngine();

	// Sub render image
	vramSetBankC(VRAM_C_SUB_BG);
	vramSetBankD(VRAM_D_SUB_SPRITE);
	vramSetBankI(VRAM_I_SUB_SPRITE);

	videoSetModeSub(MODE_5_2D |
		DISPLAY_SPR_ACTIVE |
		DISPLAY_SPR_1D);

	// Scrolling sky background
	bg_sky = bgInitSub(3, BgType_Bmp8, BgSize_B8_512x256, 1, 0);

	// Console top screen
	consoleInit(console, 0, BgType_Text4bpp, BgSize_T_256x256, 4, 0, false, true);

	// Initialize the game
	Game::getGame()->init();

	// Initialize the scene
	initLevel();

	// Ortho frustum
	scene->initFrustum(true);

	// 3D on bottom
	lcdMainOnBottom();

	// Init audio
	initAudio();

	// Set update function
	InputManager::getInputManager()->setUpdateFunction(&handleInput);

	//Initialize texture data
	initTextures();

	//Initialize the camera - ortho
	initCamera(false);

	//Initialize the enemies
	initEnemies();

	//Set up update function
	scene->setUpdateFunction(&update);

	//Main loop
	while (true)
	{
		//START FRAME
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		//INPUTS
		InputManager::getInputManager()->update();

		// 2D RENDERING

		// Top Screen - render the background using DMA
		dmaCopy(background_topBitmap, bgGetGfxPtr(bg_sky), background_topBitmapLen);
		dmaCopy(background_topPal, BG_PALETTE_SUB, background_topPalLen);

		// Render sprites
		Game::getGame()->update();

		// 3D RENDERING
		// Build the rendering of the scene
		renderStart(scene);

		//Update our camera
		updateCamera();

		// Render the scene
		renderEnd(scene);
	}

	return 0;
}

// Handles input
void handleInput(InputManager *input)
{
	// Starting a new game or skipping the intro sequence - make sure no bullets are on screen
	if (state == TITLE || 
		(state == LOSE && titleScroll <= 64) ||
		(state == WIN && titleScroll <= 64))
	{
		if (input->keyPressed(START) || input->keyPressed(A))
			startGame();
	}
	else if (state == INTRO)
	{
		if (input->keyPressed(A) || input->keyPressed(B))
			skipIntro();
	}

	// Pause/Unpause the game
	if (state == MAIN || state == PAUSE)
	{
		// Press start to toggle pause
		if (input->keyPressed(START))
			togglePause();
	}

	// Main game controls - you may fly after you win
	if (state == MAIN || state == WIN)
	{
		// Moving the ship up
		if (input->keyHeld(UP))
		{
			if (pos.y < 0.8f)
			{
				pos.y += 0.02f;
				
				if(pos.y < 0.6f)
					rot.z += 2;
			}
		}

		// Moving the ship down
		if (input->keyHeld(DOWN))
		{
			if (pos.y > 0.3f)
			{
				pos.y -= 0.02f;
				
				if(pos.y > 0.5f)
					rot.z -= 2;
			}
		}

		// Moving the ship away from the player
		if (input->keyHeld(LEFT))
		{
			rot.x -= 5;
			rot.y -= 2;	
			Vector3f scale = player.scale();
			scale.x -= 0.001f;
			scale.y -= 0.001f;
			scale.z -= 0.001f;
			if (scale.x > 0.025f)
			{
				pos.z -= 0.01f;
				player.setScale(scale);
			}
		}
		else
		{
			if (rot.x < 0)
				rot.x += 5;
		}

		// Moving the ship towards the player
		if (input->keyHeld(RIGHT))
		{
			rot.x += 5;
			rot.y += 2;
			Vector3f scale = player.scale();
			scale.x += 0.001f;
			scale.y += 0.001f;
			scale.z += 0.001f;
			if (scale.x < 0.1f)
			{
				player.setScale(scale);
				pos.z += 0.01f;
			}
		}
		else 
		{
			if (rot.x > 0)
				rot.x -= 5;
		}

		// List rotation in y back to 0
		if (rot.y < 0)
			rot.y++;
		if (rot.y > 0)
			rot.y--;

		// List rotation in z back to 0
		if (rot.z < 0)
			rot.z++;
		if (rot.z > 0)
			rot.z--;

		// Verify rotations are within bounds
		// x
		if (rot.x < -40)
			rot.x = -40;
		else if (rot.x > 40)
			rot.x = 40;
		// y
		if (rot.y < -20)
			rot.y = -20;
		else if (rot.y > 20)
			rot.y = 20;
		// z
		if (rot.z < -20)
			rot.z = -20;
		else if (rot.z > 20)
			rot.z = 20;

		// These functions do not work if you win the game
		if (state == MAIN)
		{
			// Bullets
			if (input->keyPressed(A))
				shootBullet(true);
			else if (input->keyHeld(A))
				shootBullet(false);

			// Force pause when lid closed
			if (input->lidClosed())
			{
				state = PAUSE;
				pauseScroll = 32;
				Game::getGame()->getSpriteEntry(PAUSE_OAM_ID)->y = pauseScroll;
				musicVol = 0;
				AudioManager::getAudioManager()->setModVolume(musicVol);
				AudioManager::getAudioManager()->playEffect(sfx_pause);
			}
		}
	}
}

void resetPlayer()
{
	// Player position
	pos = POS_DEFAULT;
	rot = Vector3i(0, 0, 0);

	// Player health
	health = MAX_HEALTH;

	// Level progress
	distanceLeft = LEVEL_SIZE;
}

//Update the camera data
void updateCamera()
{
	// Apply the rotation
	glRotateX(cam_rot.y);
	glRotateY(cam_rot.x);

	// Move to our camera position
	glTranslate3f32(floattof32(-cam_pos.x), floattof32(-cam_pos.y), floattof32(-cam_pos.z));
}

// Mian update function
void update(float deltaTime)
{
	// Clear text
	consoleClear();
	// Update 3D engine
	update3D();
	// Update 2D engine
	update2D();
}

// 3D update function
void update3D()
{
	// Scroll 3D sky
	sky_verts[0].setTexturePosition(Vector2i(scrollX, 0));
	sky_verts[1].setTexturePosition(Vector2i(scrollX, 257));
	sky_verts[2].setTexturePosition(Vector2i(257 + scrollX, 257));
	sky_verts[3].setTexturePosition(Vector2i(257 + scrollX, 0));
	
	// Player position
	player.setPosition(pos);
	// Player always faces a bit away from the camera
	rot.y -= 10;
	player.setRotation(rot);

	// Shadow position
	shadow.setPosition(Vector3f(pos.x, 0.2f - (1.f * player.scale().z), pos.z));
	shadow.setRotation(Vector3i(rot.x / 2, rot.y / 2, 0));
	shadow.setScale(Vector3f(player.scale().x * 2.f / (pos.y / 0.2f), player.scale().y, player.scale().z * 2.f));

	// Reset rotation
	rot.y += 10;

	// State checkers
	if (state == INTRO)
	{
		// Fly ship into position
		if (pos.x < 0.1f)
			pos.x += 0.01f;
		else
			pos.x = 0.1f;
	}

	if (state != PAUSE)
	{
		// Update bullets - in all states so there are no hanging bullets
		if (firedBullets > 0)
		{
			// Update positions
			for (int i = 0; i < BULLET_COUNT; i++)
			{
				if (bullets[i].active)
				{
					// Collision detections
					// Have we shot the enemy?
					for (int j = 0; j < ENEMY_COUNT; j++)
					{
						if (enemies[j].active)
						{
							Bullet &bullet = bullets[i];
							Enemy &enemy = enemies[j];
							if (state == MAIN &&
								bullet.position.x >= enemy.position.x &&
								bullet.position.y >= enemy.position.y - enemy.scale.y &&
								bullet.position.y <= enemy.position.y + enemy.scale.y &&
								bullet.position.z >= enemy.position.z - enemy.scale.z &&
								bullet.position.z <= enemy.position.z + enemy.scale.z)
							{
								// Kill the enemy
								enemy.dead = true;
								// Play sound
								AudioManager::getAudioManager()->playEffect(sfx_enemy_hit);
								// Remove and hide bullet
								firedBullets--;
								bullet.active = false;
								bullet.position = player.position();
								bullet.position.x = -1.f;
								bullet.scale = player.scale();
							}
						}
					}

					if(!boss.dead)
					{
						// Boss detection
						Bullet &bullet = bullets[i];
						if (state == MAIN &&
							bullet.position.x >= boss.position.x &&
							bullet.position.y >= boss.position.y - boss.scale.y &&
							bullet.position.y <= boss.position.y + boss.scale.y &&
							bullet.position.z >= boss.position.z - boss.scale.z &&
							bullet.position.z <= boss.position.z + boss.scale.z)
						{
							// Hit the boss
							boss_health -= BULLET_DAMAGE;
							// Play sound
							AudioManager::getAudioManager()->playEffect(sfx_enemy_hit);
							// Remove and hide bullet
							firedBullets--;
							bullet.active = false;
							bullet.position = player.position();
							bullet.position.x = -1.f;
							bullet.scale = player.scale();
							// Is the boss dead?
							if (boss_health <= 0)
							{
								boss.dead = true;
							}
						}
					}

					// We double check that it is still active in case it collided wth an enemy
					if (bullets[i].active)
					{
						// Is it offscreen?
						if (bullets[i].position.x > 1.f)
						{
							// Remove from active missiles
							firedBullets--;
							// Hide
							bullets[i].active = false;
							bullets[i].position = player.position();
							bullets[i].position.x = -1.f;
							bullets[i].scale = player.scale();
						}
						else
						{
							// Update it
							bullets[i].position.x += (0.5f * bullets[i].scale.x);
						}
					}
				}

				// Reposition
				bullets[i].gameObject.setPosition(bullets[i].position);

				// Rescaling
				bullets[i].gameObject.setScale(bullets[i].scale);
			}
		}

		// Update enemies
		for (int i = 0; i < ENEMY_COUNT; i++)
		{
			if (enemies[i].active)
			{
				Enemy &enemy = enemies[i];
				enemy.position.x -= 0.01f;

				// Dead enemies float to bottom then become inactive
				if (enemy.dead)
				{
					// Move to bottom
					enemy.position.y -= enemy.scale.y;

					// If bottom, they die
					if (enemy.position.y < 0.2f)
						enemy.position.y = 0.2f;
				}

				// Hide offscreen enemies
				if (enemy.position.x < -0.2f)
				{
					// Deactive and de-spawn
					enemy.active = false;
					spawnedEnemies--;
				}
				else
				{
					// Continue with update
					// Have we collided with the player?
					Vector3f scale = player.scale();
					if (!enemy.dead &&
						state == MAIN &&
						enemy.position.x <= pos.x + (2.5f * scale.x) &&
						enemy.position.x >= pos.x &&
						enemy.position.y <= pos.y + scale.y &&
						enemy.position.y >= pos.y - scale.y &&
						enemy.position.z <= pos.z + (3.f * scale.z) &&
						enemy.position.z >= pos.z - (3.f * scale.z))
					{
						// Deactive and de-spawn
						enemy.dead = true;
						// Play sound
						AudioManager::getAudioManager()->playEffect(sfx_enemy_collide);
						// Decrement health
						health -= ENEMY_DAMAGE;
					}

					// Spinning enemies
					if (!enemy.dead)
					{
						enemy.rotation.x += 5;
						enemy.rotation.x %= 360;
						if (enemy.rotation.z > 0)
							enemy.rotation.z--;
					}
					else
					{
						// Rotate to flat
						if (enemy.rotation.z < 90)
							enemy.rotation.z++;
					}

					// Update info in game object
					enemy.gameObject.setPosition(enemy.position);
					enemy.gameObject.setRotation(enemy.rotation);
					enemy.gameObject.children()[0]->setRotation(Vector3i(-enemy.rotation.x + 5, -enemy.rotation.y + 5, enemy.rotation.z));

					// Update info in shadow - if not lost
					if (state == LOSE)
					{
						enemy.shadow.setPosition(Vector3f(-0.2f, 0.25f, 5.f));
					}
					else
					{
						// Depth leads into shadow position
						float shadowHeight = 0.25f;
						shadowHeight += (5.f - enemy.position.z) / 10.f;
						enemy.shadow.setPosition(Vector3f(enemy.position.x + 0.01f, shadowHeight, 5.f));
						enemy.shadow.setRotation(Vector3i(20, 10 + enemy.rotation.x, 0));
					}
				}
			}
		}
	}

	// If in main game, spawn enemies
	if (state == MAIN)
	{
		// If still in the main level
		if (distanceLeft > 0)
		{
			// Get a random number based on distance in level
			int rnd = rand() % distanceLeft;

			// Spawn enemy if in threshold
			if (rnd < 50)
			{
				spawnEnemy();
			}
		}
		else if (!boss.active)
		{
			// We also want to play the boss music
			if (musicVol > 2)
			{
				musicVol -= 2;
			}
			// Make sure all minions are gone
			else if(spawnedEnemies == 0)
			{
				// Change song
				musicVol = GAME_VOL;
				AudioManager::getAudioManager()->setModVolume(musicVol);
				AudioManager::getAudioManager()->playMod(MOD_BOSS, MM_PLAY_LOOP);
				// Spawn the boss
				spawnBoss();
			}
		}
		else
		{
			if (boss.dead)
			{
				// Move off screen to the left
				if (boss.position.x > -2.f)
					boss.position.x -= 0.01f;

				// Crash to bottom
				if (boss.position.y > 0.2f)
					boss.position.y -= 0.01f;
				else if (boss.position.y == 0.2f)
				{
					// Play dead sound
					boss.position.y = 0.195f;
					AudioManager::getAudioManager()->playEffect(sfx_crash);
					musicVol -= 2;
				}
				else if(boss.position.y != 0.195f)
				{
					boss.position.y = 0.2f;
				}
				else if(musicVol < GAME_VOL && musicVol > 2)
				{
					musicVol -= 2;
					AudioManager::getAudioManager()->setModVolume(musicVol);
				}
				else if (musicVol < GAME_VOL && musicVol >= 2)
				{
					// Play win music
					musicVol = GAME_VOL;
					AudioManager::getAudioManager()->setModVolume(musicVol);
					AudioManager::getAudioManager()->playMod(MOD_END, MM_PLAY_LOOP);
					boss.active = false;
					state = WIN;
				}
			}
			else // BOSS ALIVE
			{
				// Boss is present - is he in the right X coordinate?
				if (boss.position.x > 0.8f)
				{
					boss.position.x -= 0.01f;
				}
				else
				{
					// Move to target
					if (boss.position.y < target.y - 0.01f)
					{
						boss.position.y += 0.01f;
					}
					else if (boss.position.y > target.y + 0.01f)
					{
						boss.position.y -= 0.01f;
					}
					else
					{
						boss.position.y = target.y;
					}
					if (boss.position.z < target.z - 0.01f)
					{
						boss.position.z += 0.01f;
					}
					else if (boss.position.z > target.z + 0.01f)
					{
						boss.position.z -= 0.01f;
					}
					else
					{
						boss.position.z = target.z;
					}

					// Recalculate scale
					boss.scale.x = 0.05f * (boss.position.z / 5.f);
					boss.scale.y = 0.05f * (boss.position.z / 5.f);
					boss.scale.z = 0.05f * (boss.position.z / 5.f);

					// Have we reached the target
					if (boss.position.y == target.y &&
						boss.position.z == target.z &&
						moveTimer-- <= 0)
					{
						// Need new target
						target.y = 0.3f + ((rand() % 5) / 10.f);

						// Random depth
						target.z = 5.f - ((rand() % 75) / 100.f);

						// Reset move timer
						moveTimer = MOVE_TIME;
					}
				}

				// Boss rotates slowly
				boss.rotation.x++;
				boss.rotation.x %=360;

				// Spawn minions
				if (spawnTimer-- <= 0)
				{
					spawnTimer = currentSpawnTime--;
					if (currentSpawnTime < MIN_SPAWN_TIME)
						currentSpawnTime = MIN_SPAWN_TIME;
					spawnEnemy(true);
				}
			}

			// Boss shadow
			float shadowHeight = 0.3f;
			shadowHeight += (5.f - boss.position.z) / 10.f;
			boss.shadow.setPosition(Vector3f(boss.position.x + 0.01f, shadowHeight, 5.f));

			// Update boss game object
			boss.gameObject.setPosition(boss.position);
			boss.gameObject.setRotation(boss.rotation);
			boss.gameObject.setScale(boss.scale);
		}
	}

	// Are we dead?
	if (state == LOSE)
	{
		// Have plane crash
		if (pos.y > 0.18f)
		{
			pos.y -= 0.01f;
			if (pos.y <= 0.18f)
			{
				// Crashed - play sound
				AudioManager::getAudioManager()->playEffect(sfx_crash);
			}
		}
		else if (pos.x > POS_DEFAULT.x)
		{
			pos.x -= 0.02f;
		}
	}
}

//2D update function
void update2D()
{
	// Audio manager reference
	AudioManager *audio = AudioManager::getAudioManager();

	// Always updating functions
	if (state != PAUSE)
	{
		// Scroll the sky background if not paused
		scrollX++;
		if (scrollX >= 256)
			scrollX = 0;
		REG_BG3X_SUB = (scrollX + 1) << 8;
	}

	// Scroll overlay sprites
	if (state == TITLE || state == LOSE || state == WIN)
	{
		// Player must be off screen to show title info
		if (pos.x <= POS_DEFAULT.x || state == WIN)
		{
			// Show death count - if there is one
			if (state != WIN && deaths > 0)
			{
				printf("\x1b[18;12HDeaths: %i", deaths);
			}
			else if (state == WIN)
			{
				printf("\x1b[18;12HYou win!");
			}

			// Scroll main title in
			if (titleScroll > 64)
			{
				titleScroll -= 2;
			}
			else 
			{
				// We don't show the press start text until the title is scrolled in
				if (textOn) // Flash "Press Start" Text
					printf("\x1b[21;10H[Press START]");
				// Decrement timer
				flashTime--;
				if (flashTime <= 0)
				{
					textOn = !textOn;
					if (!textOn)
						flashTime = TEXT_FLASH_TIME / 2;
					else
						flashTime = TEXT_FLASH_TIME;
				}
			}

			// Update title image sprite
			Game::getGame()->getSpriteEntry(TITLE_OAM_ID)->x = titleScroll;
		}
	}

	if (state == INTRO)
	{
		// Slide title away
		if (titleScroll > -128)
		{
			titleScroll -= 4;
			Game::getGame()->getSpriteEntry(TITLE_OAM_ID)->x = titleScroll;
		}
		else// Done scrolling
			titleScroll = -128;

		// Fade out title music
		if (musicVol > 0)
			musicVol -= 2;
		else // Done fading
			musicVol = 0;

		// Set the music
		audio->setModVolume(musicVol);

		// If all moving/fading is done...
		if (titleScroll == -128 && pos.x == 0.1f && musicVol == 0)
			skipIntro();
	}
	
	if (state == PAUSE)
	{
		// Slide pause down
		if (pauseScroll < 32)
		{
			pauseScroll += 8;
			if (pauseScroll > 32)
				pauseScroll = 32;
			Game::getGame()->getSpriteEntry(PAUSE_OAM_ID)->y = pauseScroll;
		}
		// Decrease volume
		if (musicVol > 0)
		{
			musicVol -= 5;
			audio->setModVolume(musicVol);
		}
	}
	
	if (state == MAIN)
	{
		// Update text and progress variables
		textTime--;
		if (distanceLeft > 0)
			distanceLeft--;

		// Are we dead?
		if (health <= 0.f)
		{
			// YES - play death sound and change state
			state = LOSE;
			deaths++;
			audio->playEffect(sfx_die);
			mainTheme = true;
			// Reset flash text variables
			textOn = true;
			flashTime = TEXT_FLASH_TIME;
		}
	}
	
	if (state == LOSE)
	{
		// Flash you lose on screen


		// Have volume decrease and then play end song
		if (mainTheme && pos.x <= POS_DEFAULT.x)
		{
			// Volume decreases fast
			musicVol -= 2;
			if (musicVol > 2)
			{
				// If volume is still valid, set it
				audio->setModVolume(musicVol);
			}
			else
			{
				// No longer fading - back at main title
				mainTheme = false;
				musicVol = GAME_VOL;
				audio->setModVolume(musicVol);
				audio->playMod(MOD_TITLE, MM_PLAY_LOOP);
			}
		}
	}

	// Text will stil render when paused
	if (state == PAUSE || state == MAIN)
	{	
		// Info text to display
		if (textTime > 0)
		{
			printf("\x1b[20;2H");
			printf(displayText.data());
		}
		else
		{
			/*
				HEALTH BARS
				- the health bars have 10 segments
			*/

			// Player health
			printf("\x1b[20;2HPlayer HP: [");

			// Output segments
			int segments = (health / MAX_HEALTH) * 10;
			for (int i = 0; i < 10; i++)
			{
				if (i < segments)
					printf("=");
				else
					printf(" ");
			}
			// End segment bar
			printf("]");

			// Level progress or boss health
			if (distanceLeft > 0)
			{
				// Progress at top
				printf("\x1b[22;2HDistance to boss: %i", distanceLeft);
			}
			else
			{
				// Boss HP
				printf("\x1b[22;4HBoss HP: [");

				// Output segments
				int segments = (boss_health / BOSS_HEALTH_MAX) * 10;
				for (int i = 0; i < 10; i++)
				{
					if (i < segments)
						printf("=");
					else
						printf(" ");
				}
				// End segment bar
				printf("]");
			}
		}
	}
}

// Starts the game
void startGame()
{
	// We are now introducing the player - slides down to bottom screen
	state = INTRO;

	// Set up the player
	resetPlayer();
}

// Skips the intro sequence
void skipIntro()
{
	// Audio manager ref
	AudioManager *audio = AudioManager::getAudioManager();
	// Just start the main game already
	state = MAIN;
	// Hide title card
	titleScroll = 256;
	Game::getGame()->getSpriteEntry(TITLE_OAM_ID)->x = titleScroll;
	// Play main game song
	musicVol = GAME_VOL;
	audio->setModVolume(musicVol);
	audio->playMod(MOD_MAIN, MM_PLAY_LOOP);	
	// Player position
	pos = Vector3f(0.1f, 0.5f, 5.f);
	// Show initial instructions
	showMessage(" Destroy the enemy boss!\n\n  | A - shoot | D-Pad - move |", 200);
}

// Toggles pausing the game
void togglePause()
{
	// Audio reference
	AudioManager *audio = AudioManager::getAudioManager();

	// Toggle states
	if (state == MAIN)
	{
		state = PAUSE;
		audio->playEffect(sfx_pause);
	}
	else if (state == PAUSE && pauseScroll >= 32)
	{
		state = MAIN;
		pauseScroll = -96;
		Game::getGame()->getSpriteEntry(PAUSE_OAM_ID)->y = pauseScroll;
		musicVol = GAME_VOL;
		audio->setModVolume(musicVol);
		audio->playEffect(sfx_unpause);
	}
}

void shootBullet(bool pressed)
{
	// Do we have a bullet available and is our shot time ready to shoot
	if (firedBullets < BULLET_COUNT)
	{
		// Either the timer is up or the user pressed the button
		if (pressed || bulletTimer-- <= 0)
		{
			// Reset the timer
			bulletTimer = BULLET_COOLDOWN;

			// Shoot the next missile - find the next non-active one first
			int m = -1; // The id of the missile to shoot
			for (int i = 0; i < BULLET_COUNT; i++)
			{
				if (!bullets[i].active)
				{
					m = i;
					break;
				}
			}

			// Ensure we have an id
			if (m > -1)
			{
				// Position and scale based on the player
				bullets[m].position.x = pos.x + (2.5f * player.scale().x);
				bullets[m].position.y = pos.y - (0.1f * player.scale().y);
				bullets[m].position.z = pos.z;
				bullets[m].scale = player.scale();
				bullets[m].active = true;

				// Play sound effect
				AudioManager::getAudioManager()->playEffect(sfx_bullet);

				// Increment counter
				firedBullets++;
			}
		}
	}
}

void spawnEnemy(bool boss_enemy)
{
	// Do we have an enemy available to spawn?
	if (spawnedEnemies < ENEMY_COUNT)
	{
		// Shoot the next enemy - find the next non-active one first
		int e = -1; // The id of the enemy to spawn
		for (int i = 0; i < ENEMY_COUNT; i++)
		{
			if (!enemies[i].active)
			{
				e = i;
				break;
			}
		}

		// Ensure we have an id
		if (e > -1)
		{
			// Position and scale randomly
			Enemy &enemy = enemies[e];

			// Activate
			enemy.active = true;
			enemy.dead = false;

			// Just off screen
			enemy.position.x = 1.2f;

			// Random initial rotation
			enemy.rotation.x = rand() % 360;

			// Always facing a bit towards the camera
			enemy.rotation.y = 30;

			if (boss_enemy)
			{
				enemy.position.x = boss.position.x;
				enemy.position.y = boss.position.y;
				enemy.position.z = boss.position.z;
			}
			else
			{
				// Random height
				enemy.position.y = 0.3f + ((rand() % 6) / 10.f);

				// Random depth
				enemy.position.z = 5.f - ((rand() % 75) / 100.f);
			}

			// Adjust scale based on depth
			float depth = enemy.position.z;
			enemy.scale.x = 0.05f * (depth / 5.f);
			enemy.scale.y = 0.05f * (depth / 5.f);
			enemy.scale.z = 0.05f * (depth / 5.f);

			// Adjust shadow scale
			float shadowScale = 0.75f * (depth / 5.f);
			enemy.shadow.setScale(Vector3f(shadowScale, shadowScale, shadowScale));

			// Set game object data
			enemy.gameObject.setPosition(enemy.position);
			enemy.gameObject.setScale(enemy.scale);
			enemy.gameObject.setRotation(enemy.rotation);

			// Increment counter
			spawnedEnemies++;
		}
		
	}
}

void showMessage(std::string message, int duration)
{
	// Overwrite whatever is currently in our text vars
	textTime = duration;
	displayText = message;
}

void spawnBoss()
{
	// Remove all enemies
	for (int i = 0; i < ENEMY_COUNT; i++)
	{
		Enemy &enemy = enemies[i];
		enemy.dead = false;
		enemy.active = false;
		enemy.position.x = -0.21f;
		enemy.gameObject.setPosition(enemy.position);
		enemy.shadow.setPosition(enemy.position);
	}

	// Reset boss
	boss.rotation = Vector3i(0, 350, 0);
	boss.scale = Vector3f(0.05f, 0.05f, 0.05f);
	boss.position = Vector3f(1.1f, 0.5f, 5.f);

	// Boss position and target
	boss.gameObject.setPosition(boss.position);
	boss.gameObject.setRotation(boss.rotation);
	boss.gameObject.setScale(boss.scale);
	target = boss.position;

	// Active boss
	boss.active = true;
	boss.dead = false;
}

// Init audio
void initAudio()
{
	// Initialize audio manager and load audio data
	AudioManager *audio = AudioManager::getAudioManager();
	audio->init();

	// Load songs
	audio->loadMod(MOD_TITLE);
	audio->loadMod(MOD_MAIN);
	audio->loadMod(MOD_BOSS);
	audio->loadMod(MOD_END);
	audio->setModVolume(musicVol);

	// Load effects
	sfx_pause = audio->loadEffect(SFX_PAUSE, BITRATE_DEFAULT, HANDLE_DEFAULT, VOL_MID);
	sfx_unpause = audio->loadEffect(SFX_UNPAUSE, BITRATE_DEFAULT, HANDLE_DEFAULT, VOL_MID);
	sfx_bullet = audio->loadEffect(SFX_BULLET, BITRATE_DEFAULT, HANDLE_DEFAULT, VOL_MID);
	sfx_enemy_collide = audio->loadEffect(SFX_ENEMY_COLLIDE, BITRATE_DEFAULT, HANDLE_DEFAULT, VOL_MID);
	sfx_enemy_hit = audio->loadEffect(SFX_ENEMY_HIT, BITRATE_DEFAULT, HANDLE_DEFAULT, VOL_MID);
	sfx_die = audio->loadEffect(SFX_DIE);
	sfx_crash = audio->loadEffect(SFX_CRASH);

	// Start playing module
	audio->playMod(MOD_TITLE, MM_PLAY_LOOP);
}

// Initialize the level
void initLevel()
{
	// Initialize player variables
	resetPlayer();

	// Sets up camera
	cam_pos = Vector3f(0.5f, 0.5f, 0.5f);

	// Rotations
	cam_rot.x = 0;
	cam_rot.y = 0;

	// Reference to scene levels
	BaseObject *base = mainScene.baseObject();

	// Ground
	Polygon *ground_poly = new Polygon();
	ground_poly->addVertex(Vector3f(0.f, 0.45f, -5.f), ColorRGB(0, 200, 0));
	ground_poly->addVertex(Vector3f(0.f, 0.f, 5.f), ColorRGB(0, 100, 0));
	ground_poly->addVertex(Vector3f(1.f, 0.f, 5.f), ColorRGB(0, 100, 0));
	ground_poly->addVertex(Vector3f(1.f, 0.45f, -5.f), ColorRGB(0, 200, 0));
	ground.addPolygon(ground_poly);

	// Sky
	Polygon *sky_poly = new Polygon();
	sky_poly->setTexture(&tex_sky);
	// Verts
	sky_verts[0].setPosition(Vector3f(0.f, 1.f, -1.f));
	sky_verts[0].setTexturePosition(Vector2i(0, 0));
	sky_verts[1].setPosition(Vector3f(0.f, 0.f, -1.f));
	sky_verts[1].setTexturePosition(Vector2i(0, 257));
	sky_verts[2].setPosition(Vector3f(1.f, 0.f, -1.f));
	sky_verts[2].setTexturePosition(Vector2i(257, 257));
	sky_verts[3].setPosition(Vector3f(1.f, 1.f, -1.f));
	sky_verts[3].setTexturePosition(Vector2i(257, 0));
	sky_poly->addVertex(&sky_verts[0]);
	sky_poly->addVertex(&sky_verts[1]);
	sky_poly->addVertex(&sky_verts[2]);
	sky_poly->addVertex(&sky_verts[3]);
	sky.addPolygon(sky_poly);

	// Player
	Polygon *poly_body = new Polygon();
	poly_body->setType(GL_TRIANGLES);
	Polygon *poly_wing = new Polygon();
	poly_wing->setType(GL_TRIANGLES);
	poly_wing->setBackRender(true);
	// Rear of plane
	Vertex *centerBack = new Vertex();
	centerBack->setPosition(Vector3f(0.25f, 0.f, 0.f));
	centerBack->setColor(ColorRGB(150, 150, 150));
	// top left
	poly_body->addVertex(Vector3f(0.f, 0.5f, 0.f), ColorRGB(225, 225, 225));
	poly_body->addVertex(Vector3f(0.f, 0.f, -1.f), ColorRGB(225, 225, 225));
	poly_body->addVertex(centerBack);
	// bottom left
	poly_body->addVertex(Vector3f(0.f, 0.f, -1.f), ColorRGB(225, 225, 225));
	poly_body->addVertex(Vector3f(0.f, -0.5f, 0.f), ColorRGB(225, 225, 225));
	poly_body->addVertex(centerBack);
	// bottom right
	poly_body->addVertex(Vector3f(0.f, -0.5f, 0.f), ColorRGB(225, 225, 225));
	poly_body->addVertex(Vector3f(0.f, 0.f, 1.f), ColorRGB(225, 225, 225));
	poly_body->addVertex(centerBack);
	// top right
	poly_body->addVertex(Vector3f(0.f, 0.f, 1.f), ColorRGB(225, 225, 225));
	poly_body->addVertex(Vector3f(0.f, 0.5f, 0.f), ColorRGB(225, 225, 225));
	poly_body->addVertex(centerBack);
	// MAIN BODY
	// top left
	poly_body->addVertex(Vector3f(3.f, 0.f, 0.f), ColorRGB(150, 150, 150));
	poly_body->addVertex(Vector3f(0.f, 0.f, -1.f), ColorRGB(150, 150, 150));
	poly_body->addVertex(Vector3f(0.f, 0.5f, 0.f), ColorRGB(150, 150, 150));
	// top right
	poly_body->addVertex(Vector3f(3.f, 0.f, 0.f), ColorRGB(130, 130, 130));
	poly_body->addVertex(Vector3f(0.f, 0.5f, 0.f), ColorRGB(130, 130, 130));
	poly_body->addVertex(Vector3f(0.f, 0.f, 1.f), ColorRGB(130, 130, 130));
	// bottom left
	poly_body->addVertex(Vector3f(3.f, 0.f, 0.f), ColorRGB(130, 130, 130));
	poly_body->addVertex(Vector3f(0.f, -0.5f, 0.f), ColorRGB(130, 130, 130));
	poly_body->addVertex(Vector3f(0.f, 0.f, -1.f), ColorRGB(130, 130, 130));
	// bottom right
	poly_body->addVertex(Vector3f(3.f, 0.f, 0.f), ColorRGB(150, 150, 150));
	poly_body->addVertex(Vector3f(0.f, 0.f, 1.f), ColorRGB(150, 150, 150));
	poly_body->addVertex(Vector3f(0.f, -0.5f, 0.f), ColorRGB(150, 150, 150));
	// WINGS
	// right
	Vertex *wing_r_a = new Vertex();
	wing_r_a->setPosition(Vector3f(2.f, 0.05f, 0.f));
	wing_r_a->setColor(ColorRGB(200, 200, 200));
	Vertex *wing_r_b = new Vertex();
	wing_r_b->setPosition(Vector3f(0.5f, 0.25f, 0.f));
	wing_r_b->setColor(ColorRGB(200, 200, 200));
	Vertex *wing_r_c = new Vertex();
	wing_r_c->setPosition(Vector3f(0.5f, 0.25f, 1.5f));
	wing_r_c->setColor(ColorRGB(180, 180, 180));
	Vertex *wing_r_d = new Vertex();
	wing_r_d->setPosition(Vector3f(0.5f, 0.f, 0.f));
	wing_r_d->setColor(ColorRGB(160, 160, 160));
	// left
	Vertex *wing_l_a = new Vertex();
	wing_l_a->setPosition(Vector3f(2.f, 0.05f, 0.f));
	wing_l_a->setColor(ColorRGB(200, 200, 200));
	Vertex *wing_l_b = new Vertex();
	wing_l_b->setPosition(Vector3f(0.5f, 0.25f, 0.f));
	wing_l_b->setColor(ColorRGB(200, 200, 200));
	Vertex *wing_l_c = new Vertex();
	wing_l_c->setPosition(Vector3f(0.5f, 0.25f, -1.5f));
	wing_l_c->setColor(ColorRGB(180, 180, 180));
	Vertex *wing_l_d = new Vertex();
	wing_l_d->setPosition(Vector3f(0.5f, 0.f, 0.f));
	wing_l_d->setColor(ColorRGB(160, 160, 160));
	// top
	poly_wing->addVertex(wing_r_a);
	poly_wing->addVertex(wing_r_b);
	poly_wing->addVertex(wing_r_c);
	poly_wing->addVertex(wing_l_a);
	poly_wing->addVertex(wing_l_c);
	poly_wing->addVertex(wing_l_b);
	// bottom
	poly_wing->addVertex(wing_r_a);
	poly_wing->addVertex(wing_r_d);
	poly_wing->addVertex(wing_r_c);
	poly_wing->addVertex(wing_l_a);
	poly_wing->addVertex(wing_l_d);
	poly_wing->addVertex(wing_l_c);
	// back
	poly_wing->addVertex(wing_r_b);
	poly_wing->addVertex(wing_r_d);
	poly_wing->addVertex(wing_r_c);
	poly_wing->addVertex(wing_l_b);
	poly_wing->addVertex(wing_l_c);
	poly_wing->addVertex(wing_l_d);
	// Add polygons to object
	player.addPolygon(poly_body);
	player.addPolygon(poly_wing);
	player.setScale(Vector3f(0.1f, 0.1f, 0.1f));

	// SHADOW
	Polygon *poly_shadow = new Polygon();
	poly_shadow->setBackRender(true);
	poly_shadow->setType(GL_TRIANGLES);
	// left
	poly_shadow->addVertex(Vector3f(1.5f, 0.f, 0.f), ColorRGB(0, 0, 0));
	poly_shadow->addVertex(Vector3f(0.f, 0.f, -0.5f));
	poly_shadow->addVertex(Vector3f(0.f, 0.25f, 0.f), ColorRGB(0, 0, 0));
	// right
	poly_shadow->addVertex(Vector3f(1.5f, 0.f, 0.f), ColorRGB(0, 0, 0));
	poly_shadow->addVertex(Vector3f(0.f, 0.25f, 0.f));
	poly_shadow->addVertex(Vector3f(0.f, 0.f, 0.5f), ColorRGB(0, 0, 0));
	shadow.addPolygon(poly_shadow);
	shadow.setPosition(Vector3f(-1.f, -1.f, 0.f));

	// MISSILES
	// There are multiple missiles
	for (int i = 0; i < BULLET_COUNT; i++)
	{
		Object *object_bullet = new Object();
		Polygon *poly_bullet = new Polygon();
		// body
		poly_bullet->addVertex(Vector3f(0.1f, 0.2f, 0.f), ColorRGB(0, 0, 0));
		poly_bullet->addVertex(Vector3f(0.1f, 0, 0.f));
		poly_bullet->addVertex(Vector3f(0.4f, 0, 0.f));
		poly_bullet->addVertex(Vector3f(0.4f, 0.2f, 0.f), ColorRGB(0, 0, 0));
		poly_bullet->addVertex(Vector3f(0.1f, 0.2f, 0.f), ColorRGB(200, 0, 0));
		poly_bullet->addVertex(Vector3f(0.f, 0.2f, 0.f));
		poly_bullet->addVertex(Vector3f(0.f, 0, 0.f));
		poly_bullet->addVertex(Vector3f(0.1f, 0, 0.f), ColorRGB(200, 0, 0));
		// add polygons
		object_bullet->addPolygon(poly_bullet);
		// Set game objects
		bullets[i].gameObject.addChild(object_bullet);
		// Position them off-screen to the left
		bullets[i].position = player.position();
		bullets[i].position.x = -1.f;
		bullets[i].scale = player.scale();
		bullets[i].gameObject.setPosition(bullets[i].position);
		bullets[i].gameObject.setScale(bullets[i].scale);
		// Not active
		bullets[i].active = false;
		// Add to base object
		base->addChild(&bullets[i].gameObject);
	}

	// Add objects to scene
	base->addChild(&ground);
	base->addChild(&sky);
	base->addChild(&shadow);
	base->addChild(&player);
}

void initTextures()
{
	// Initialize the texture manager object
	TextureManager *textures = TextureManager::getTextureManager();

	// Sky texture
	tex_sky.setSize(TEXTURE_SIZE_512, TEXTURE_SIZE_256);
	tex_sky.setLength(background_botBitmapLen);
	tex_sky.setData((u8*)background_botBitmap);
	textures->addTexture(&tex_sky);

	// Clear texture
	tex_clear.setSize(TEXTURE_SIZE_8, TEXTURE_SIZE_8);
	tex_clear.setLength(clearBitmapLen);
	tex_clear.setData((u8*)clearBitmap);
	textures->addTexture(&tex_clear);

	// We have all textures in the manager, commit them
	textures->commitTextures();

	// Set the clear texture
	scene->renderer()->setClearTexture(&tex_clear);
}

// Initialize the enemies - random stuff happens here
void initEnemies()
{
	// Seed rand
	srand(time(NULL));

	// Color vars
	uint8 r, g, b;

	for (int i = 0; i < ENEMY_COUNT; i++)
	{
		// Get reference to this new enemy
		Enemy &enemy = enemies[i];

		// Polygon for the enemy to render
		Polygon *p = new Polygon();
		p->setType(GL_TRIANGLES);
		p->setBackRender(true);

		// Random color
		r = rand() % 100;
		g = rand() % 100;
		b = rand() % 100;

		// Triangle
		p->addVertex(Vector3f(0.f, 1.f, 0.f), ColorRGB(r, g, b));
		p->addVertex(Vector3f(0.f, -1.f, -1.f), ColorRGB(r, g, b));
		p->addVertex(Vector3f(0.f, -1.f, 1.f), ColorRGB(r, g, b));

		// Add to enemy
		enemy.gameObject.addPolygon(p);

		// Polygon for the enemy shadow
		Polygon *s = new Polygon();
		s->setType(GL_TRIANGLES);
		s->setBackRender(true);
		
		// Triangle
		s->addVertex(Vector3f(0.1f, -0.2f, 0.f), ColorRGB(0, 0, 0));
		s->addVertex(Vector3f(0.f, -0.2f, 0.f), ColorRGB(0, 0, 0));
		s->addVertex(Vector3f(0.f, -0.2f, 0.1f), ColorRGB(0, 0, 0));

		// Add to enemy
		enemy.shadow.addPolygon(s);

		// Randomly orient the enemy
		enemy.position = Vector3f(2.f, 0.f, 0.f);
		enemy.scale = Vector3f(0.02f, 0.02f, 0.02f);
		enemy.rotation = Vector3i(0, 20, 0);

		// Set gameobject info
		enemy.gameObject.setPosition(enemy.position);
		enemy.gameObject.setScale(enemy.scale);
		enemy.gameObject.setRotation(enemy.rotation);

		// Set shadow object info
		enemy.shadow.setPosition(Vector3f(enemy.position.x, 0.25f, 5.f));
		enemy.shadow.setRotation(Vector3i(350, 20, 0));
		enemy.shadow.setScale(Vector3f(0.75f, 0.75f, 0.75f));

		// Set other data
		enemy.active = false;

		// Add to base object
		scene->baseObject()->addChild(&enemy.gameObject);
		scene->baseObject()->addChild(&enemy.shadow);
	}

	// Initialize the boss

	// Color params
	r = 255;
	g = 0;
	b = 0;

	Polygon *e = new Polygon();
	e->setType(GL_TRIANGLES);
	Polygon *e_wing = new Polygon();
	e_wing->setBackRender(true);
	e_wing->setType(GL_TRIANGLES);

	Vertex *e_1 = new Vertex();
	e_1->setPosition(Vector3f(0.f, 1.f, 0.f));
	e_1->setColor(ColorRGB(char(0.4f * r), char(0.4f * g), char(0.4f * b)));
	Vertex *e_2 = new Vertex();
	e_2->setPosition(Vector3f(0.f, -1.f, -1.f));
	e_2->setColor(ColorRGB(char(0.1f * r), char(0.1f * g), char(0.1f * b)));
	Vertex *e_3 = new Vertex();
	e_3->setPosition(Vector3f(0.f, -1.f, 1.f));
	e_3->setColor(ColorRGB(char(0.4f * r), char(0.4f * g), char(0.4f * b)));
	Vertex *e_4 = new Vertex();
	e_4->setPosition(Vector3f(2.f, 1.f, 0.f));
	e_4->setColor(ColorRGB(char(0.625f * r), char(0.625f * g), char(0.625f * b)));
	Vertex *e_5 = new Vertex();
	e_5->setPosition(Vector3f(2.f, -1.f, -1.f));
	e_5->setColor(ColorRGB(char(0.625f * r), char(0.625f * g), char(0.625f * b)));
	Vertex *e_6 = new Vertex();
	e_6->setPosition(Vector3f(2.f, -1.f, 1.f));
	e_6->setColor(ColorRGB(char(0.625f * r), char(0.625f * g), char(0.625f * b)));
	Vertex *e_7 = new Vertex();
	e_7->setPosition(Vector3f(2.75f, 0.f, 0.f));
	e_7->setColor(ColorRGB(r, g, b));

	// front
	e->addVertex(e_7);
	e->addVertex(e_5);
	e->addVertex(e_4);
	e->addVertex(e_4);
	e->addVertex(e_6);
	e->addVertex(e_7);
	e->addVertex(e_5);
	e->addVertex(e_7);
	e->addVertex(e_6);
	// facing camera
	e->addVertex(e_6);
	e->addVertex(e_4);
	e->addVertex(e_1);
	e->addVertex(e_1);
	e->addVertex(e_3);
	e->addVertex(e_6);
	// facing away
	e->addVertex(e_1);
	e->addVertex(e_4);
	e->addVertex(e_5);
	e->addVertex(e_5);
	e->addVertex(e_2);
	e->addVertex(e_1);
	// bottom
	e->addVertex(e_2);
	e->addVertex(e_5);
	e->addVertex(e_3);
	e->addVertex(e_6);
	e->addVertex(e_3);
	e->addVertex(e_5);
	// back
	e->addVertex(e_1);
	e->addVertex(e_2);
	e->addVertex(e_3);
	// top wing
	e_wing->addVertex(Vector3f(0.f, 1.f, 0.f), ColorRGB(0, 0, 0));
	e_wing->addVertex(Vector3f(0.5f, 1.f, 0.f), ColorRGB(char(0.2f * r), char(0.2f * g), char(0.2f * b)));
	e_wing->addVertex(Vector3f(0.f, 2.f, 0.f), ColorRGB(char(0.3f * r), char(0.3f * g), char(0.3f * b)));
	e_wing->addVertex(Vector3f(0.f, 2.f, 0.f), ColorRGB(char(0.3f * r), char(0.3f * g), char(0.3f * b)));
	e_wing->addVertex(Vector3f(-1.f, 2.f, 0.f), ColorRGB(char(0.6f * r), char(0.6f * g), char(0.6f * b)));
	e_wing->addVertex(Vector3f(0.f, 1.5f, 0.f), ColorRGB(char(0.3f * r), char(0.3f * g), char(0.3f * b)));
	// left wing
	e_wing->addVertex(Vector3f(0.f, -1.f, -1.f), ColorRGB(0, 0, 0));
	e_wing->addVertex(Vector3f(0.5f, -1.f, -1.f), ColorRGB(char(0.2f * r), char(0.2f * g), char(0.2f * b)));
	e_wing->addVertex(Vector3f(0.f, -2.f, -1.6f), ColorRGB(char(0.3f * r), char(0.3f * g), char(0.3f * b)));
	e_wing->addVertex(Vector3f(0.f, -2.f, -1.6f), ColorRGB(char(0.3f * r), char(0.3f * g), char(0.3f * b)));
	e_wing->addVertex(Vector3f(-1.f, -2.f, -1.6f), ColorRGB(char(0.6f * r), char(0.6f * g), char(0.6f * b)));
	e_wing->addVertex(Vector3f(0.f, -1.5f, -1.3f), ColorRGB(char(0.3f * r), char(0.3f * g), char(0.3f * b)));
	// right wing
	e_wing->addVertex(Vector3f(0.f, -1.f, 1.f), ColorRGB(0, 0, 0));
	e_wing->addVertex(Vector3f(0.5f, -1.f, 1.f), ColorRGB(char(0.2f * r), char(0.2f * g), char(0.2f * b)));
	e_wing->addVertex(Vector3f(0.f, -2.f, 1.6f), ColorRGB(char(0.3f * r), char(0.3f * g), char(0.3f * b)));
	e_wing->addVertex(Vector3f(0.f, -2.f, 1.6f), ColorRGB(char(0.3f * r), char(0.3f * g), char(0.3f * b)));
	e_wing->addVertex(Vector3f(-1.f, -2.f, 1.6f), ColorRGB(char(0.6f * r), char(0.6f * g), char(0.6f * b)));
	e_wing->addVertex(Vector3f(0.f, -1.5f, 1.3f), ColorRGB(char(0.3f * r), char(0.3f * g), char(0.3f * b)));

	boss.gameObject.addPolygon(e);
	boss.gameObject.addPolygon(e_wing);
	boss.rotation = Vector3i(0, 350, 0);
	boss.scale = Vector3f(0.05f, 0.05f, 0.05f);
	boss.position = Vector3f(1.1f, 0.5f, 5.f);

	boss.gameObject.setPosition(boss.position);
	boss.gameObject.setRotation(boss.rotation);
	boss.gameObject.setScale(boss.scale);

	// Polygon for the enemy shadow
	Polygon *s = new Polygon();
	s->setBackRender(true);

	// Quad
	s->addVertex(Vector3f(0.1f, -0.2f, 0.f), ColorRGB(0, 0, 0));
	s->addVertex(Vector3f(0.f, -0.2f, 0.f), ColorRGB(0, 0, 0));
	s->addVertex(Vector3f(0.f, -0.2f, 0.1f), ColorRGB(0, 0, 0));
	s->addVertex(Vector3f(0.1f, -0.2f, 0.1f), ColorRGB(0, 0, 0));

	// Add to enemy
	boss.shadow.addPolygon(s);

	// Set shadow object info
	boss.shadow.setPosition(Vector3f(boss.position.x, 0.25f, 5.f));
	boss.shadow.setRotation(Vector3i(350, 20, 0));

	// Add to game
	scene->baseObject()->addChild(&boss.gameObject);
	scene->baseObject()->addChild(&boss.shadow);
}
