
@{{BLOCK(clear)

@=======================================================================
@
@	clear, 8x8@16, 
@	+ bitmap not compressed
@	Total size: 128 = 128
@
@	Time-stamp: 2016-11-25, 16:42:23
@	Exported by Cearn's GBA Image Transmogrifier, v0.8.13
@	( http://www.coranac.com/projects/#grit )
@
@=======================================================================

	.section .rodata
	.align	2
	.global clearBitmap		@ 128 unsigned chars
	.hidden clearBitmap
clearBitmap:
	.word 0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF
	.word 0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF
	.word 0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF
	.word 0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF,0x7FFF7FFF

@}}BLOCK(clear)
