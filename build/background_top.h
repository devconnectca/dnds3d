
//{{BLOCK(background_top)

//======================================================================
//
//	background_top, 512x224@8, 
//	+ palette 256 entries, not compressed
//	+ bitmap not compressed
//	Total size: 512 + 114688 = 115200
//
//	Time-stamp: 2016-11-25, 16:42:22
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.13
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_BACKGROUND_TOP_H
#define GRIT_BACKGROUND_TOP_H

#define background_topBitmapLen 114688
extern const unsigned int background_topBitmap[28672];

#define background_topPalLen 512
extern const unsigned short background_topPal[256];

#endif // GRIT_BACKGROUND_TOP_H

//}}BLOCK(background_top)
