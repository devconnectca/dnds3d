
//{{BLOCK(pause)

//======================================================================
//
//	pause, 64x64@4, 
//	+ palette 256 entries, not compressed
//	+ 64 tiles not compressed
//	Total size: 512 + 2048 = 2560
//
//	Time-stamp: 2016-11-25, 16:42:23
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.13
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_PAUSE_H
#define GRIT_PAUSE_H

#define pauseTilesLen 2048
extern const unsigned int pauseTiles[512];

#define pausePalLen 512
extern const unsigned short pausePal[256];

#endif // GRIT_PAUSE_H

//}}BLOCK(pause)
