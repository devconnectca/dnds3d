
//{{BLOCK(clear)

//======================================================================
//
//	clear, 8x8@16, 
//	+ bitmap not compressed
//	Total size: 128 = 128
//
//	Time-stamp: 2016-11-25, 16:42:23
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.13
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_CLEAR_H
#define GRIT_CLEAR_H

#define clearBitmapLen 128
extern const unsigned int clearBitmap[32];

#endif // GRIT_CLEAR_H

//}}BLOCK(clear)
