
//{{BLOCK(title)

//======================================================================
//
//	title, 64x64@4, 
//	+ palette 256 entries, not compressed
//	+ 64 tiles not compressed
//	Total size: 512 + 2048 = 2560
//
//	Time-stamp: 2016-11-25, 16:42:23
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.13
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_TITLE_H
#define GRIT_TITLE_H

#define titleTilesLen 2048
extern const unsigned int titleTiles[512];

#define titlePalLen 512
extern const unsigned short titlePal[256];

#endif // GRIT_TITLE_H

//}}BLOCK(title)
