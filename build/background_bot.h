
//{{BLOCK(background_bot)

//======================================================================
//
//	background_bot, 512x256@16, 
//	+ bitmap not compressed
//	Total size: 262144 = 262144
//
//	Time-stamp: 2016-11-25, 16:42:22
//	Exported by Cearn's GBA Image Transmogrifier, v0.8.13
//	( http://www.coranac.com/projects/#grit )
//
//======================================================================

#ifndef GRIT_BACKGROUND_BOT_H
#define GRIT_BACKGROUND_BOT_H

#define background_botBitmapLen 262144
extern const unsigned int background_botBitmap[65536];

#endif // GRIT_BACKGROUND_BOT_H

//}}BLOCK(background_bot)
