#ifndef __DNDS_3D_AUDIO_H
#define __DNDS_3D_AUDIO_H

#include <nds.h>
#include <stdlib.h>
#include <stdio.h>
#include <maxmod9.h>

#include "../build/soundbank.h"
#include "../build/soundbank_bin.h"

// Easier names to remember
typedef mm_sound_effect SoundEffect;
typedef mm_pmode PlayMode;

// Bitrates for audio
enum Bitrate
{
	BITRATE_DEFAULT = (short unsigned int)(1.0f * (1 << 10))
};

// Handles for audio
enum Handle
{
	HANDLE_DEFAULT = 0
};

// Volumes for audio
enum Volume
{
	VOL_MUTE = 0,
	VOL_LOW = 63,
	VOL_MID = 127,
	VOL_HIGH = 195,
	VOL_MAX = 255
};

// Panning audio
enum Panning
{
	PAN_LEFT = 0,
	PAN_LEFT_MID = 63,
	PAN_CENTER = 127,
	PAN_RIGHT_MID = 195,
	PAN_RIGHT = 255
};

/*
	Audio Manager
	- Handles music and
		sound effects
	- Requires existing audio
		data to compile:
		'soundbank.h' and
		'soundbank_bin.h'
*/
class AudioManager
{
private:
	// Self-reference (Singleton)
	static AudioManager *s_instance;
	// Data values for audio managing
	char mod_volume, mod_tempo, mod_pitch;
	char fx_volume;
private:
	// Deconstructor
	AudioManager();
public:
	// CONSTRUCTORS

	// Singleton accessor
	static AudioManager *getAudioManager();
	// Deconstructor
	~AudioManager();

	// MEMBER FUNCTIONS

	/*
		Loads a sound effect into memory
		- Non-default values for the input
			parameters can be found above
	*/
	SoundEffect *loadEffect(const char fxFile,
		const short unsigned int bitrate = BITRATE_DEFAULT,
		const short unsigned int handle = HANDLE_DEFAULT,
		const char volume = VOL_MAX, 
		const char panning = PAN_CENTER);

	/*
		Loads a MOD file into audio memory
		- for modFile, place the defined
			version of your song file
			(Found in soundbank.h)
			ex: MOD_SONG
	*/
	void loadMod(const char modFile);

	// Initialize the maxmod module
	void init();

	// Play effect with default settings
	void playEffect(const char effectFile);

	// Play effect with modified settings
	void playEffect(SoundEffect *effect);

	// Play a loaded mod (PlayMode = MM_PLAY_ONCE or MM_PLAY_LOOP)
	void playMod(const char modFile, const PlayMode playMode = MM_PLAY_ONCE);

	// Pause a mod
	void pauseMod();

	// Resume a mod
	void resumeMod();

	// Stop a mod
	void stopMod();

	// SETTERS

	void setfxVolume(const char volume);

	void setModPitch(const char pitch);
	void setModTempo(const char tempo);
	void setModVolume(const char volume);

	// GETTERS

	const char fxVolume();

	const char modPitch();
	const char modTempo();
	const char modVolume();
	
};


#endif