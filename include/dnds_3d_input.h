#ifndef __DNDS_3D_INPUT_H
#define __DNDS_3D_INPUT_H

#include <nds.h>

/*
	List of all buttons to check
*/
enum Button
{
	A,
	B,
	X,
	Y,
	L,
	R,
	UP,
	DOWN,
	LEFT,
	RIGHT,
	SELECT,
	START,
	TOUCHPAD,
	LID
};

/*
	Input Manager
	- handles checking updates in key presses
		and the touch screen
*/
class InputManager
{
private:
	//Self-reference (Singleton)
	static class InputManager *s_instance;
	// State of the keys that are down
	uint32 _keysDown;
	// State of the keys that are up
	uint32 _keysUp;
	// State of the keys that are held
	uint32 _keysHeld;
	// Touch position
	touchPosition _touchPosition;
	// User defined input function to call after update
	void(*_updateFunction)(InputManager*);
private:
	// Default constructor
	InputManager();
	// Function to convert from Button to libnds Button types
	uint32 keyFromButton(const Button button);
public:
	// CONSTRUCTORS

	// Singleton accessor
	static InputManager *getInputManager();

	// Deconstructor
	~InputManager();

	// MEMBER FUNCTIONS

	// Checks if the button was just pressed
	const bool keyPressed(const Button button);

	// Checks if the button is now up
	const bool keyUp(const Button button);

	// Checks if the button has been held
	const bool keyHeld(const Button button);

	// Checks if the screen was just touched
	const bool screenTouched();

	// Checks if the screen is being dragged along
	const bool screenDragged();

	// Did the lid just close
	const bool lidClosed();

	// Did the lid just open
	const bool lidOpened();

	// Returns the current touch position value
	const touchPosition getTouchPosition();

	// Scans state of keys and updates registers (Call first each frame)
	void update();

	// Sets the function to call at the end of each update() call
	void setUpdateFunction(void(*updateFunction)(InputManager*));
};

#endif