#ifndef __GAME_H
#define __GAME_H

// Include all sprite headers
#include "spritefunc.h"
#include "dnds_3d_scenes.h"

// Include sprite images
#include "../build/pause.h"
#include "../build/title.h"

// Constants for sprites
static const int TITLE_OAM_ID = 0;
static const int PAUSE_OAM_ID = 1;

// Sprite render constants
static const int BYTES_PER_16_COLOR_TILE = 32;
static const int COLORS_PER_PALETTE = 16;
static const int BOUNDARY_VALUE = 32;
static const int OFFSET_MULTIPLIER = BOUNDARY_VALUE / sizeof(SPRITE_GFX[0]);

// Game state
enum State
{
	TITLE,
	INTRO,
	MAIN,
	PAUSE,
	LOSE,
	WIN
};

// Game structures

// Player Bullets
struct Bullet
{
	Object gameObject;
	Vector3f position;
	Vector3f scale;
	bool active;
};

// Enemies
struct Enemy
{
	Object gameObject;
	Object shadow;
	Vector3f position;
	Vector3i rotation;
	Vector3f scale;
	bool active;
	bool dead;
};

// GAME singleton

class Game
{
private:
	// Self-reference (Singleton)
	static Game *s_instance;
	// Sprite data
	SpriteInfo spriteInfo[SPRITE_COUNT];
	OAMTable *oam;
private:
	// Constructor
	Game();
	// Initialize sprites
	void initSprites();
public:
	// Get reference to this game
	static Game *getGame();
	// Get oam ref
	OAMTable *getOAM();
	// Initialize the game
	void init();
	// Update the game (renders too)
	void update();
	// Get sprite info for specific OAM ID
	SpriteInfo *getSpriteInfo(const u16 oamId);
	// Get sprite entry for specific OAM ID
	SpriteEntry *getSpriteEntry(const u16 oamId);
};

#endif