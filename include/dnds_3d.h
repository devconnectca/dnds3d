#ifndef __DNDS_3D_H
#define __DNDS_3D_H

/*----------------------------
	OVERVIEW
------------------------------

DNDS_3D stands for:
	Dan's NDS 3D Engine

A basic 3D engine for the
Nintendo DS running off of
libNDS.

----------------------------*/

/*----------------------------
	ENGINE HEADERS
----------------------------*/

#include "dnds_3d_anim.h"
#include "dnds_3d_input.h"
#include "dnds_3d_audio.h"

/*----------------------------
	DEFINITIONS
----------------------------*/

//Maximum vertices that can be rendered each frame
#define MAX_VERTICES 6000

//Function used to initialize the engine
void initEngine();

//Sets the clear color
void setClearColor(ColorRGB color);

//Function used to initialize the camera
void initCamera(bool perspective);

//Function used to start a scene render
void renderStart(Scene *scene);

//Function used to complete a scene render
void renderEnd(Scene *scene);

#endif