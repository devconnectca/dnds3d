#ifndef __DNDS_3D_ANIM_H
#define __DNDS_3D_ANIM_H

// Since animations deal with vertices, we need the scene header
#include "dnds_3d_scenes.h"

// Forward Declaration

class Animation;
class Keyframe;


/*
	Animation
*/
class Animation
{
private:
	// List of the keyframes
	Keyframe **_keyframes;

	// Number of keyframes in this animation
	int _keyframeCount;

	// Current animated keyframe
	int _currentKeyframe;

	// Are we looping the animation?
	bool _looping;

	// Is the animation playing?
	bool _playing;

	// Chained animation to play after this one finishes
	Animation *_chained;
public:
	// CONSTRUCTORS

	// Default constructor
	Animation();
	// Deconstructor
	~Animation();

	// MEMBER FUNCTIONS
	
	// Add a keyframe
	void addKeyframe(Keyframe *keyframe);

	// Controls functions
	// Starts - resumes the animation
	void play();
	// Pauses the animation (Does nothing if already paused)
	void pause();
	// Stops and resets the animation to initial state
	void stop();
	// Progress the animation, process info
	void update();

	// GETTERS

	const bool playing();

	// SETTERS

	void setChained(Animation *chained);
	void setLooping(const bool looping);

};

/*
	Keyframe
*/
class Keyframe
{
private:
	// The references to the vertices to animation
	Vertex **_vertices;

	// Amount to move vertex positions by to reach target each update
	Vector3f *_increments;

	// Final target positions for vertices when keyframe is reached
	Vector3f *_targets;

	// Number of vertices (same as increment and target counts)
	int _vertexCount;

	// Position used for calculations
	Vector3f position;

	// Function to play at the end of the keyframe
	void(*_endFunction)();
public:
	// Number of times update must be called to reach keyframe
	int _frameLength;

	// Current # of update calls into this keyframe
	int _frameTime;
public:
	// CONSTRUCTORS

	// Default constructor
	Keyframe();
	// Constructor that sets frame length
	Keyframe(const int frameLength);
	// Deconstructor
	~Keyframe();

	// MEMBER FUNCTIONS

	// Adds a vertex to animate with a target position
	void addVertex(Vertex *vertex, const Vector3f target);

	// Calculates the increment data
	void calculateIncrements();

	// Update information
	void update();

	// Run the end function
	void runEndFunction();

	// GETTERS

	const bool finished();

	// SETTERS

	void setEndFunction(void(*endFunction)());
	void setFrameLength(const int frameLength);
	void setFrameTime(const int frameTime);
};

#endif