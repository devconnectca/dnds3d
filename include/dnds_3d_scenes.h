#ifndef __DNDS_3D_SCENES_H
#define __DNDS_3D_SCENES_H

/*
	This header file creates the classes necessary to
	form Scenes. These Scenes can be populated with
	SceneNodes.
*/

/*----------------------------
	INTERNAL HEADERS
----------------------------*/

//Headers required for texture data and the libnds libraries
#include "dnds_3d_textures.h"

/*----------------------------
	DEFINITIONS
----------------------------*/
#define MAX_VERTICES 6000
#define DEFAULT_FRAMETIME 30

/*--------------------------------
	STRUCTS AND TYPE DEFINITIONS
--------------------------------*/

/*
	POLYGON_TYPE
	- alias for GL_GLBEGIN_ENUM
*/
typedef GL_GLBEGIN_ENUM POLYGON_TYPE;

/*
	Vector2i
	- stores a 2D vector of integers
	- mainly used for texture coordinates
*/
typedef struct Vector2i
{
	int x, y;
	Vector2i()
	{
		x = 0;
		y = 0;
	}
	Vector2i(const int _x, const int _y)
	{
		x = _x;
		y = _y;
	}
} Vector2i;

/*
	Vector3i
	- stores a 3D vector of integers
	- mainly used for rotation degrees
*/
typedef struct Vector3i
{
	int x, y, z;
	Vector3i()
	{
		x = 0;
		y = 0;
		z = 0;
	}
	Vector3i(const int _x, const int _y, const int _z)
	{
		x = _x;
		y = _y;
		z = _z;
	}
} Vector3i;

/*
	ColorRGB
	- same as Vector3b, easier to
		understand variable names
*/
typedef struct ColorRGB
{
	uint8 r, g, b;
	ColorRGB()
	{
		r = 0;
		g = 0;
		b = 0;
	}
	ColorRGB(const uint8 _r, const uint8 _g, const uint8 _b)
	{
		r = _r;
		g = _g;
		b = _b;
	}
} ColorRGB;

/*
	Vector3b
	- stores a 3D vector of bytes
	- used mainly for rgb color
		(See struct Color)
*/
typedef struct Vector3b
{
	uint8 x, y, z;
	Vector3b()
	{
		x = 0;
		y = 0;
		z = 0;
	}
	Vector3b(const uint8 _x, const uint8 _y, const uint8 _z)
	{
		x = _x;
		y = _y;
		z = _z;
	}
} Vector3b;

/*
	Vector3f
	- stores a 3D vector of floats
	- used mainly for vertex coordinates
*/
typedef struct Vector3f
{
	float x, y, z;
	Vector3f()
	{
		x = 0.f;
		y = 0.f;
		z = 0.f;
	}
	Vector3f(const float _x, const float _y, const float _z)
	{
		x = _x;
		y = _y;
		z = _z;
	}
} Vector3f;

/*----------------------------
	CLASSES AND INTERFACES
----------------------------*/

//Forward Declarations (In order of definition)
class Scene;
class BaseObject;
class Object;
class Polygon;
class Vertex;
class Renderer;
class Room;
class Portal;

/*
	Scene
	- The scene (scene graph) holds all data for a
		particular scene (Instantiate multiple
		scenes sparingly)
	- It contains a single base object that can
		not be removed
*/
class Scene
{
private:
	//The main holder of data for the scene
	BaseObject *_baseObject;
	//The renderer
	Renderer *_renderer;
	/*
		Our view frustum:
			0: FarTopLeft
			1: FarBottomLeft
			2: FarBottomRight
			3: FarTopRight
			4: NearTopLeft
			5: NearBottomLeft
			6: NearBottomRight
			7: NearTopRight
	*/
	Vector3f frustum[8];
	Object *frustum_object;
	Polygon *frustum_polygon;
	// Update function (The float is deltaTime)
	void (*_updateFunction)(float);
	// How long each frame lasts
	float _frameTime;
	// How many milliseconds have passed since last frame
	int _frameCount;
	// The current room to render, if this is NULL, renderer ignores optimizations
	Room *_room;
public:
	// CONSTRUCTORS

	Scene();
	Scene(BaseObject *base);
	~Scene();

	//MEMBER FUNCTIONS

	void clean();
	void clearFrustum();
	void countFrame();
	bool hasUpdateFunction();
	void initFrustum(bool ortho = false);
	void resetFrameCount();
	void update(const float deltaTime);

	//GETTERS

	BaseObject *baseObject();
	Object *frustumObject();
	const float frameCount();
	const float frameTime();
	const float frustumBound(const int index);
	Renderer *renderer();
	Room *room();

	//SETTERS

	void setBaseObject(BaseObject *base);
	void setFrameTime(const float frameTime);
	void setRoom(Room *room);
	void setUpdateFunction(void(*updateFunction)(float));
};

/*
	BaseObject
	- Fundamental super class of all scene elements
	- Contains all polygon data, texture data, bounding
		box data and an array of child objects
	- There can only be one base object per scene
*/
class BaseObject
{
protected:
	//Polygon data
	Polygon **_polygons;
	int _polygonCount;

	//Child object data
	Object **_children;
	int _childCount;

	//Do we want to rotate based on position? or in place?
	bool _positionRotate;

	// Shoud we render this object?
	bool _visible;

	/*
		Bound box data
			0: smallest X
			1: largest X
			2: smallest Y
			3: largest Y
			4: smallest Z
			5: largest Z
	*/
	float bounds[6];
	bool boundsSet;
	//The location of the object relative to its parent
	Vector3f _position;
	//The scale factor to be applied to vertices before rendering
	Vector3f _scale;
	//The rotation about the center of this object to rotate about
	Vector3i _rotation;
protected:
	// Must be called within this class or Object classes only
	void calculateVertices(Vector3f scale, Vector3i rotation, Vector3f position);
public:
	// CONSTRUCTORS

	BaseObject();
	~BaseObject();

	// MEMBER FUNCTIONS
	
	void addChild(Object *child);
	void addPolygon(Polygon *polygon);
	void calculateBounds(Vector3f *position);
	void calculateVertices();
	void removeChild(Object *child);
	void removeChild(const int index);
	void removePolygon(Polygon *polygon);
	void removePolygon(const int index);

	// GETTERS

	const float bound(const int index);
	const int childCount();
	Object **children();
	const int polygonCount();
	Polygon **polygons();
	const bool positionRotate();

	const Vector3f position();
	const Vector3f scale();
	const Vector3i rotation();

	const bool visible();

	// SETTERS

	void setBound(const int index, const float value);
	void setPositionRotation(const bool inplace);
	void setPosition(const Vector3f offset);
	void setScale(const Vector3f scale);
	void setRotation(const Vector3i rotation);
	void setVisibility(const bool visible);
};

/*
	Object
	- Child class of BaseObject
	- These objects act as children of 
		each other or the scene's base
		object
*/
class Object : public BaseObject
{
private:
	// The parent object ref
	Object *_parent;
	BaseObject *_root;
public:
	// CONSTRUCTORS
	Object();

	// MEMBER FUNCTIONS
	
	void addChild(Object *child);
	void calculateBounds(Vector3f *position);
	void calculateVertices(Vector3f scale, Vector3i rotation, Vector3f position);

	// GETTERS
	
	Object *parent();
	BaseObject *root();

	// SETTERS

	void setParent(Object *parent);
	void setRoot(BaseObject *root);
};

/*
	Polygon
	- Stores vertex data that is
		stored in an object
	- Each vertex may also store a
		color or texture coordinate
	- May reference a texture
*/
class Polygon
{
private:
	//Referenced texture
	Texture *_texture;
	//Vertex data
	Vertex **_vertices;
	int _vertexCount;
	//The type of OpenGL polygon to render
	POLYGON_TYPE _type;
	//Do we render the the poly in reverse as well?
	bool _backRender;
public:
	// CONSTRUCTORS

	Polygon();
	Polygon(const Polygon &polygon);
	Polygon(const POLYGON_TYPE type);
	Polygon(Texture *texture);
	Polygon(Texture *texture, const POLYGON_TYPE type);
	~Polygon();

	// VERTEX ADDERS
	void addVertex(Vertex *vertex);
	void addVertex(const Vector3f position);
	void addVertex(const Vector3f position, const Vector2i texturePosition);
	void addVertex(const Vector3f position, const ColorRGB color);
	void addVertex(const Vector3f position, const Vector2i texturePosition, const ColorRGB color);

	// GETTERS

	const bool backRender();
	const POLYGON_TYPE type();
	Texture *texture();
	Vertex **vertices();
	const int vertexCount();

	// SETTERS

	void setBackRender(const bool backRender);
	void setTexture(Texture *texture);
	void setType(const POLYGON_TYPE type);
};

/*
	Vertex
	- Stores mandatory object-position data
	- May store a texture coordinate to map to
		(This requires the vertex's parent
		polygon to reference a texture)
	- May also store a color
*/
class Vertex
{
private:
	//Raw position relative to polygon's parent object
	Vector3f _position;
	
	//Calculated render position
	Vector3f _renderPosition;

	/*
		Position coordinate of texture
		- This is a pointer to save memory if
			not used
	*/
	Vector2i *_texturePosition;
	/*
		Color to set when this vertex is rendered
		- This is a pointer to save memory if
			not used
	*/
	ColorRGB *_color;
	/*
		Render boolean used to check if this
		vertex has been recalculated this frame
	*/
	bool _calculated;
	/*
		Referenced boolean indicated if this
		vertex was create specifically for a
		polygon, or was referenced
	*/
	bool _referenced;
public:
	// CONSTRUCTORS

	Vertex();
	Vertex(const Vector3f position);
	Vertex(const Vector3f position, const Vector2i texturePosition);
	Vertex(const Vector3f position, const ColorRGB color);
	Vertex(const Vector3f position, const Vector2i texturePosition, const ColorRGB color);
	~Vertex();

	// GETTERS

	const bool calculated();
	const Vector3f position();
	const bool referenced();
	const Vector3f renderPosition();
	const Vector2i texturePosition();
	const ColorRGB color();

	// SETTERS

	void setCalculated(const bool calculated);
	void setPosition(const Vector3f position);
	void setReferenced(const bool referenced);
	void setRenderPosition(const Vector3f renderPosition);
	void setTexturePosition(const Vector2i texturePosition);
	void setColor(const ColorRGB color);

	// CHECK REFERENCES

	bool texturePositionSet();
	bool colorSet();
};

/*
	Renderer
	- created each frame to render
		vertex data
*/
class Renderer
{
private:
	//TODO change to priority queue
	Polygon **polygons;
	int polygonCount;
	// Clear texture
	Texture *clearTexture;
public:
	// CONSTRUCTORS

	Renderer();
	~Renderer();

	// MEMBER FUNCTIONS

	void addPolygon(Polygon *polygon);
	void clean();
	void render();
	void setClearTexture(Texture *texture);
};

/*
	Room
	- Defines a render room
	- If the scene is referencing a room,
		only objects in this room will be 
		considered for rendering (Based
		on the scene frustum as well)
	- If the frustum contains any portal(s) to
		another room, we will call render on
		those rooms as well
*/
class Room
{
private:
	// Portal data
	Portal **_portals;
	int _portalCount;
	// Object data
	Object *_mainObject;
	// Has this room been rendered yet
	bool _rendered;
public:
	// CONSTRUCTORS

	// Default constructor
	Room();
	// Deconstructor
	~Room();

	// MEMBER FUNCTIONS

	/*
		Add Portal
		- Creates a new portal connecting this
			room to another one
		@param connected
			- The room we are connecting to
		@param container
			- The object containing bounding
				data for this portal
	*/
	void addPortal(Portal *portal);

	// GETTERS

	Object *mainObject();
	const int portalCount();
	Portal **portals();
	const bool rendered();

	// SETTERS

	/*
		Set Main Object
		- Sets the render object for this room
		- Best practice is to keep polygon data
			empty for this object and fill only
			the children with objects contained
			within this room
		- Portal objects do not have to be here
			but including them will not make a
			difference
	*/
	void setMainObject(Object *mainObject);

	// Set value of rendered variable
	void setRendered(const bool rendered);
};

/*
	Portal
	- Connects a room to another room
	- References an object containing
		the bounding information of the
		portal
*/
class Portal
{
private:
	Room *_connected;
	Object *_container;
public:
	// CONSTRUCTORS

	// Default constructor
	Portal();
	// Deconstructor
	~Portal();

	// GETTERS

	// Get the connected room
	Room *connected();
	// Get the container object
	Object *container();

	// SETTERS

	// Sets the connected room
	void setConnected(Room *connected);
	// Sets the container object
	void setContainer(Object *container);
};

#endif