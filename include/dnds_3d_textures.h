#ifndef __DNDS_3D_TEXTURES_H
#define __DNDS_3D_TEXTURES_H

/*
	This file creates the texture management/storage class
	When a user wishes to load in custom texture files, they
	will pass pointers to the texture data to be loaded
*/

/*----------------------------
	EXTERNAL HEADERS
----------------------------*/

//Global PI definition for trig
#define PI 3.141592654

//Headers required for using the libNDS structs and functions
#include <nds.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>

//Forward class declarations
class TextureManager;
class Texture;

/*
	TextureManager
	- Manages all textures for the engine
	- Add your texture data BEFORE you COMMIT textures
	- This is a singleton, to use it, place the
		following code early on in your application:
		TextureManager *TextureManager::s_instance = 0;
*/
class TextureManager
{
private:
	//Self-reference (Singleton)
	static TextureManager *s_instance;
	//Stores the texture data
	Texture **textures;
	int textureCount;
	int *textureIDs;
	//Constructor
	TextureManager();
public:
	//Singleton accessor
	static TextureManager *getTextureManager();
	//Deconstructor
	~TextureManager();
	//Adds a texture to the manager
	void addTexture(Texture *texture);
	//Puts texture data into VRAM
	bool commitTextures();
};

/*
	Texture
	- stores texture data (and TEX_COORDS)
*/
class Texture
{
private:
	//The texture identifier
	int *_id;
	//The length of the texture bitmap
	int _length;
	//The texture data
	u8* _data;
	//Sizes
	GL_TEXTURE_SIZE_ENUM _width;
	GL_TEXTURE_SIZE_ENUM _height;
	//Colour type
	GL_TEXTURE_TYPE_ENUM _type;
public:
	//Constructor
	Texture();
	//Return int value of the id
	const int id();
	//Return pointer to data
	u8* data();
	//Return value of length
	const int length();
	//Maps the texture to the id and into VRAM
	bool map(int *id);
	//Sets the location of the texture data
	void setData(u8* data);
	//Sets the length data
	void setLength(int length);
	//Sets dimensions
	void setSize(GL_TEXTURE_SIZE_ENUM width, GL_TEXTURE_SIZE_ENUM height);
	//Sets colour type
	void setType(GL_TEXTURE_TYPE_ENUM type);
};

#endif