#ifndef __SPRITEFUNC_H
#define __SPRITEFUNC_H

#include <nds.h>
#include <nds/arm9/sprite.h>

static const int SPRITE_DMA_CHANNEL = 3;

typedef struct {
	int oamID;
	int width;
	int height;
	int angle;
	SpriteEntry * entry;
} SpriteInfo;

void updateOAM(OAMTable *oam);

void initOAMTable(OAMTable *oam);

void moveSprite(SpriteEntry *spriteEntry, u16 x, u16 y);

void scaleSprite(SpriteRotation *spriteRotation, int sx, int sy);

void rotateSprite(SpriteRotation *spriteRotation, int angle);

void setPriority(SpriteEntry *spriteEntry, ObjPriority priority);

void setSpriteVisibility(SpriteEntry *spriteEntry, bool hidden, bool affine, bool doubleBound);

#endif